﻿using openGl_Control.Math_Funcs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control
{
    public enum AngleType : int
    {
        Radiano = 1,
        Degradiano = 2,
        Cosseno = 4,
        Seno = 8,
        Tangente = 16
    }

    public class Angle
    {
        private double m_degradiano;

        public Angle(AngleType tipo, double valor)
        {
            AlteraValor(tipo, valor);
        }

        public Angle(double valor)
            : this(AngleType.Degradiano, valor)
        {
        }

        public Angle(AngleType tipo)
            : this(tipo, 0)
        {
        }

        public Angle()
            : this(AngleType.Degradiano, 0)
        {
        }

        public static implicit operator Angle(double valor)
        {
            return new Angle(AngleType.Degradiano, valor);
        }

        public static implicit operator Angle(int valor)
        {
            return new Angle(AngleType.Degradiano, (double)valor);
        }

        public static implicit operator double (Angle angulo)
        {
            return angulo.Degradianos;
        }

        public void AlteraValor(AngleType tipo, double valor)
        {
            switch (tipo)
            {
                //case AngleType.Cosseno:
                //    Cosseno = valor;
                //    break;
                case AngleType.Degradiano:
                    Degradianos = valor;
                    break;
                case AngleType.Radiano:
                    Radianos = valor;
                    break;
                //case AngleType.Seno:
                //    Seno = valor;
                //    break;
                //case AngleType.Tangente:
                //    Tangente = valor;
                //    break;
            }

        }

        public double Cosseno
        {
            get
            {
                return Math.Cos(Radianos);
            }
            set
            {
                if (value < -1)
                    value = -1;
                else if (value > 1)
                    value = 1;
                Radianos = Math.Acos(value);
            }
        }

        public double Seno
        {
            get
            {
                return Math.Sin(Radianos);
            }
            set
            {
                if (value < -1)
                    value = -1;
                else if (value > 1)
                    value = 1;
                Radianos = Math.Asin(value);
            }
        }

        public double Tangente
        {
            get
            {
                return Math.Tan(Radianos);
            }
            set
            {
                Radianos = Math.Atan(value);
            }
        }

        public double Radianos
        {
            get
            {
                return MathFuncs.Rad(this.Degradianos);
            }
            set
            {
                double pi2 = 2 * Math.PI;
                double radianos = value % pi2;

                if (radianos < 0)
                    radianos += pi2;

                Degradianos = MathFuncs.Deg(radianos);
            }
        }

        public double Degradianos
        {
            get
            {
                return m_degradiano;
            }
            set
            {
                m_degradiano = value % 360;

                if (m_degradiano < 0)
                    m_degradiano += 360d;
            }
        }

        public static Angle EntreDoisPontos(double x1, double y1, double x2, double y2)
        {
            return EntreDoisPontos(x1, y1, x2, y2, false);
        }


        public static Angle EntreDoisPontos(double x1, double y1, double x2, double y2, bool considerarProximosZero)
        {
            var anguloRetorno = new Angle();

            var diffX = x2 - x1;
            var diffY = y2 - y1;

            //Gabriel: Retirei o teste se o diffX estava bem proximo de zero para setar automaticamente o angulo como 90 ou 180. Estava causando inclinação em planos que tinham angulo entre 0 e 1

            var anguloRadiano = Math.Abs(diffY) < 0.00001 && Math.Abs(diffX) < 0.00001 ? 0 : Math.Atan(diffY / diffX);
            if (diffX < 0)
                anguloRadiano += Math.PI;

            if (!considerarProximosZero && Math.Abs(anguloRadiano - 0) <= 0.00001f)
                anguloRadiano = 0;

            anguloRetorno.AlteraValor(AngleType.Radiano, anguloRadiano);

            return anguloRetorno;
        }


        //public static Angle EntreDoisPontos(Linha linha)
        //{
        //    return Angle.EntreDoisPontos(linha.PontoI.X, linha.PontoI.Y, linha.PontoF.X, linha.PontoF.Y);
        //}


        public static Angle EntreDoisPontos(Point2D ponto1, Point2D ponto2)
        {
            return Angle.EntreDoisPontos(ponto1.X, ponto1.Y, ponto2.X, ponto2.Y);
        }

        public static Angle operator -(Angle ang1, Angle ang2)
        {
            return new Angle(AngleType.Degradiano, (ang1.Degradianos - ang2.Degradianos));
        }

        public static Angle operator +(Angle ang1, Angle ang2)
        {
            return new Angle(AngleType.Degradiano, (ang1.Degradianos + ang2.Degradianos));
        }

        public static Angle operator +(Angle ang1, double incrementoDegradiano)
        {
            return new Angle(AngleType.Degradiano, (ang1.Degradianos + incrementoDegradiano));
        }

        public override string ToString()
        {
            return String.Concat(Degradianos.ToString(), "º");
        }

        //public static float DiferencaAngular(Linha linha1, Linha linha2)
        //{
        //    return DiferencaAngular(linha1.PontoI, linha1.PontoF, linha2.PontoI, linha2.PontoF);
        //}

        //public static float DiferencaAngular(Ponto2D vertice, Ponto2D ponto1, Ponto2D ponto2)
        //{
        //    return DiferencaAngular(vertice, ponto1, vertice, ponto2);
        //}

        //public static float DiferencaAngular(Ponto2D pontoA1, Ponto2D pontoA2, Ponto2D pontoB1, Ponto2D pontoB2)
        //{
        //    Vetor2D vetor1 = new Vetor2D(pontoA1, pontoA2);
        //    Vetor2D vetor2 = new Vetor2D(pontoB1, pontoB2);

        //    return DiferencaAngular(vetor1, vetor2);
        //}

        //public static float DiferencaAngular(Vetor2D vetor1, Vetor2D vetor2)
        //{
        //    vetor1.Normalizar();
        //    vetor2.Normalizar();

        //    Angle anguloVetor1 = EntreDoisPontos(0, 0, vetor1.A, vetor1.B);
        //    Angle anguloVetor2 = EntreDoisPontos(0, 0, vetor2.A, vetor2.B);
        //    Angle angulo = vetor1.Angulo(vetor2);

        //    float retorno = (float)angulo.Degradianos;

        //    if (FuncoesMatematicas.ValorEqual(anguloVetor1 - retorno, anguloVetor2.Degradianos, 1f))
        //        retorno *= -1;

        //    return retorno;
        //}

        //public static bool Equivalente(Angle ang1, Angle ang2)
        //{
        //    if (FuncoesMatematicas.ValorEqual(ang1.Degradianos, 0d) || FuncoesMatematicas.ValorEqual(ang1.Degradianos, 360d))
        //        return FuncoesMatematicas.ValorEqual(ang2.Degradianos, 0d) || FuncoesMatematicas.ValorEqual(ang2.Degradianos, 360d);

        //    return FuncoesMatematicas.ValorEqual(ang1.Degradianos, ang2.Degradianos);
        //}

        //public static bool EstaDentro(Angle angInicial, Angle angFinal, bool antiHorario, Angle angTeste)
        //{
        //    return EstaDentro(angInicial, angFinal, antiHorario, angTeste, null);
        //}

        //public static bool EstaDentro(Angle angInicial, Angle angFinal, bool antiHorario, Angle angTeste, double tolerancia)
        //{
        //    return EstaDentro(angInicial, angFinal, antiHorario, angTeste, (double?)tolerancia);
        //}

        //private static bool EstaDentro(Angle angInicial, Angle angFinal, bool antiHorario, Angle angTeste, double? tolerancia)
        //{
        //    double inicial = angInicial.Degradianos;
        //    double final = angFinal.Degradianos;
        //    double teste = angTeste.Degradianos;

        //    if (tolerancia == null)
        //    {
        //        if (FuncoesMatematicas.ValorEqual(inicial, teste))
        //            return true;
        //        if (FuncoesMatematicas.ValorEqual(final, teste))
        //            return true;
        //    }
        //    else
        //    {
        //        if (FuncoesMatematicas.ValorEqual(inicial, teste, tolerancia.Value))
        //            return true;
        //        if (FuncoesMatematicas.ValorEqual(final, teste, tolerancia.Value))
        //            return true;
        //    }

        //    if (antiHorario)
        //    {
        //        if (final > inicial)
        //            inicial += 360d;
        //        if (teste < final)
        //            teste += 360d;

        //        return teste < inicial;
        //    }
        //    else
        //    {
        //        if (inicial > final)
        //            final += 360d;
        //        if (teste < inicial)
        //            teste += 360d;

        //        return teste < final;
        //    }
        //}
    }
}
