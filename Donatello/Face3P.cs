﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control
{
    public class Face3P
    {
        private Point3D _p1;
        public Point3D P1
        {
            get { return _p1; }
            set { _p1 = value; }
        }

        private Point3D _p2;
        public Point3D P2
        {
            get { return _p2; }
            set { _p2 = value; }
        }

        private Point3D _p3;
        public Point3D P3
        {
            get { return _p3; }
            set { _p3 = value; }
        }

        private Vector3D _normal;
        public Vector3D Normal
        {
            get { return _normal; }
            set { _normal = value; }
        }

        private double[] _normalVector;
        public double[] NormalVector
        {
            get { return _normalVector; }
            set { _normalVector = value; }
        }

        public Face3P()
        {

        }
    }
}
