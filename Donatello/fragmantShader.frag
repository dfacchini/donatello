﻿#ifdef GL_ES
	precision mediump float;
	#endif

	uniform vec3 iResolution;   // viewport resolution (in pixels)
	uniform vec4 iMouse;        // mouse pixel coords. xy: current, zw: click
	uniform float iGlobalTime;  // shader playback time (in seconds)
	uniform float u_time;

	void main() {
		//gl_FragColor = vec4(abs(sin(u_time)),0.0,0.0,1.0);
		vec2 uv = gl_FragCoord.xy / iResolution.xy;
		gl_FragColor = vec4(uv,0.5+0.5*sin(iGlobalTime),1.0);
	}