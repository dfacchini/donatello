//#version 330

//layout (std140) uniform Matrices {
// mat4 m_pvm;
// mat4 m_viewModel;
// mat3 m_normal;
//};

//in vec4 position;

//void main()
//{
// gl_Position = m_pvm * position ;
//}

void main(void)
{
	vec4 a = gl_Vertex;
	//a.x = a.x * 0.2;
	//a.y = a.y * 0.2;
	gl_Position = gl_ModelViewProjectionMatrix * a;
}