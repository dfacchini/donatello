﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using openGl_Control.Elements;
using openGl_Control.Math_Funcs;

namespace openGl_Control
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Axes axes = new Axes();
            axes.IndexColor = 20;
            Floor floor = new Floor();
            floor.IndexColor = 50;



            //PolygonFace3P polygon3P = new PolygonFace3P();
            //polygon3P.IndexColor = 2147483640;

            Polygon genericPolygon = new Polygon() { IndexColor = 16000000 };
            List<IDrawable> drawableObjects = new List<IDrawable>();

            //Random e = new Random();

            
            //int cont = 0;

            //for (int i = 0; i < 40; i += 2)
            //{
            //    for (int j = 0; j < 80; j += 2)
            //    {
            //        for (int k = 0; k < 100; k += 2)
            //        {
            //            drawableObjects.Add(new Polygon()
            //            {
            //                IndexColor = cont,//Convert.ToInt32(string.Format("{0}{1}{2}", k, j, i)),
            //                TranslateX = k  * 50f,
            //                TranslteY = j * 50f,
            //                TranslteZ = i * 50f
            //            });

            //            cont++;
            //        }
            //    }
            //}
            //drawableObjects.Add(genericPolygon);
            //drawableObjects.Add(polygon3P);
            //drawableObjects.Add(floor);

            //drawableObjects.Add(polygon3P);
            drawableObjects.Add(genericPolygon);
            drawableObjects.Add(floor);
            drawableObjects.Add(axes);

            World world = new World(drawableObjects);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new MainOpenglControl(world));
        }
    }
}
