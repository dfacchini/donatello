﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control
{
    public struct Dimension
    {
        public float Width;
        public float Height;
        public float Depth;

        public Dimension(float width, float height, float depth)
        {
            Width = width;
            Height = height;
            Depth = depth;
        }
    }
}
