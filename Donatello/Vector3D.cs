using openGl_Control;
using openGl_Control.Math_Funcs;
using System;

namespace openGl_Control
{
    public class Vector3D
    {
        public Vector3D()
            : this(0f, 0f, 0f)
        {
        }

        public Vector3D(float a, float b, float c)
        {
            m_a = a;
            m_b = b;
            m_c = c;

            m_anguloPlano = null;
            m_anguloInclinacao = null;
        }

        public Vector3D(Angle angPlano, Angle angInclinacao)
        {
            m_anguloPlano = angPlano;
            m_anguloInclinacao = angInclinacao;

            CalcularABC();
        }

        public Vector3D(Point3D pt)
            : this(pt.X, pt.Y, pt.Z)
        {
        }

        public Vector3D(Point3D pt1, Point3D pt2)
            : this(pt2.X - pt1.X, pt2.Y - pt1.Y, pt2.Z - pt1.Z)
        {

        }

        private float m_a;
        public float A
        {
            get
            {
                return m_a;
            }
        }

        private float m_b;
        public float B
        {
            get
            {
                return m_b;
            }
        }

        private float m_c;
        public float C
        {
            get
            {
                return m_c;
            }
        }

        public Angle Alfa
        {
            get
            {
                return new Angle(AngleType.Cosseno, A / Modulo());
            }
        }

        public Angle Beta
        {
            get
            {
                return new Angle(AngleType.Cosseno, B / Modulo());
            }
        }

        public Angle Gama
        {
            get
            {
                return new Angle(AngleType.Cosseno, C / Modulo());
            }
        }

        private Angle m_anguloPlano;
        public Angle AnguloPlano
        {
            get
            {
                if (m_anguloPlano == null)
                    CalculaAnguloPlano();
                return m_anguloPlano;
            }
        }

        private Angle m_anguloInclinacao;
        public Angle AnguloInclinacao
        {
            get
            {
                if (m_anguloInclinacao == null)
                    CalculaAnguloInclinacao();
                return m_anguloInclinacao;
            }
        }

        private static Vector3D m_vetorUp;
        public static Vector3D VetorUp
        {
            get
            {
                if (m_vetorUp == null)
                    m_vetorUp = new Vector3D(0f, 0f, 1f);
                return m_vetorUp;
            }
        }

        private static Vector3D m_vetorDown;
        public static Vector3D VetorDown
        {
            get
            {
                if (m_vetorDown == null)
                    m_vetorDown = new Vector3D(0f, 0f, -1f);
                return m_vetorDown;
            }
        }

        private void CalcularABC()
        {
            float m = (float)m_anguloInclinacao.Seno;

            m_a = (float)m_anguloPlano.Cosseno * m;
            m_b = (float)m_anguloPlano.Seno * m;
            m_c = (float)m_anguloInclinacao.Cosseno;
        }

        private void CalculaAnguloPlano()
        {
            if (m_anguloPlano == null)
                m_anguloPlano = new Angle();

            m_anguloPlano.Degradianos = 0;

            if (m_a != 0)
                m_anguloPlano.Tangente = m_b / m_a;
            else
                m_anguloPlano.Degradianos = m_b > 0 ? 90 : m_b < 0 ? -90 : 0;

            if (m_a < 0)
                m_anguloPlano.Degradianos += 180;
        }

        private void CalculaAnguloInclinacao()
        {
            if (m_anguloInclinacao == null)
                m_anguloInclinacao = new Angle();

            m_anguloInclinacao.Degradianos = 0;

            float fator = (float)Math.Sqrt(m_a * m_a + m_b * m_b);

            if (m_c == 0)
                m_anguloInclinacao.Degradianos = 90;
            else
                m_anguloInclinacao.Tangente = fator / m_c;

            if (m_c < 0)
                m_anguloInclinacao.Degradianos += 180;
        }

        public void Setar(float a, float b, float c)
        {
            m_a = a;
            m_b = b;
            m_c = c;

            if (m_anguloPlano != null)
                CalculaAnguloPlano();

            if (m_anguloInclinacao != null)
                CalculaAnguloInclinacao();
        }

        public void Setar(Angle angPlano, Angle angInclinacao)
        {
            if (m_anguloPlano == null)
            {
                m_anguloPlano = new Angle(angPlano.Degradianos);
                m_anguloInclinacao = new Angle(angInclinacao.Degradianos);
            }
            else
            {
                m_anguloPlano.Degradianos = angPlano.Degradianos;
                m_anguloInclinacao.Degradianos = angInclinacao.Degradianos;
            }

            CalcularABC();
        }

        public float Modulo()
        {
            //return ( float )Math.Sqrt( Math.Pow( m_a, 2 ) + Math.Pow( m_b, 2 ) + Math.Pow( m_c, 2 ) );
            return (float)Math.Sqrt(m_a * m_a + m_b * m_b + m_c * m_c);
        }

        public Vector3D Versor()
        {
            float modulo = Modulo();

            if (modulo == 0)
                return new Vector3D(0f, 0f, 0f);
            else
                return new Vector3D(m_a / modulo, m_b / modulo, m_c / modulo);
        }

        public Angle Angulo(Vector3D u)
        {
            return Angulo(this, u);
        }

        public void Normalizar()
        {
            var versor = Versor();

            var a = versor.A;
            var b = versor.B;
            var c = versor.C;

            //Vin�cius: Sol:8392 - Angulo de parede menor que 1: Removendo as condi��es a baixo o plano fica junto � parede mas com uma inclina��o no �ngulo.
            //Gabriel: Sol: 19821 - Alterado a forma de arredondamento do vetor, para aceitar, tambem, angulos entre 0 e 1.

            if (Math.Abs(c) > 0.9999f && Math.Abs(a) < 0.001f && Math.Abs(b) < 0.001f)
            {
                a = 0f;
                b = 0f;
                c = 1f * Math.Sign(c);
            }
            else if (Math.Abs(b) > 0.9999f && Math.Abs(a) < 0.001f && Math.Abs(c) < 0.001f)
            {
                a = 0f;
                b = 1f * Math.Sign(b);
                c = 0f;
            }
            else if (Math.Abs(a) > 0.9999f && Math.Abs(c) < 0.001f && Math.Abs(b) < 0.001f)
            {
                a = 1f * Math.Sign(a);
                b = 0f;
                c = 0f;
            }

            Setar(a, b, c);
        }

        public void Rotacionar(Vector3D eixo, Angle angulo)
        {
            Vector3D eixoClone = (Vector3D)eixo.Clone();
            Angle ang = new Angle(360 - angulo.Degradianos);
            float c = (float)ang.Cosseno;
            float s = (float)ang.Seno;
            float ic = 1f - c;
            float[,] m = new float[3, 3];
            float va, vb, vc;

            eixoClone.Normalizar();

            m[0, 0] = (eixoClone.A * eixoClone.A) * ic + c;
            m[0, 1] = (eixoClone.B * eixoClone.A) * ic + (eixoClone.C * s);
            m[0, 2] = (eixoClone.C * eixoClone.A) * ic - (eixoClone.B * s);
            m[1, 0] = (eixoClone.A * eixoClone.B) * ic - (eixoClone.C * s);
            m[1, 1] = (eixoClone.B * eixoClone.B) * ic + c;
            m[1, 2] = (eixoClone.C * eixoClone.B) * ic + (eixoClone.A * s);
            m[2, 0] = (eixoClone.A * eixoClone.C) * ic + (eixoClone.B * s);
            m[2, 1] = (eixoClone.B * eixoClone.C) * ic - (eixoClone.A * s);
            m[2, 2] = (eixoClone.C * eixoClone.C) * ic + c;

            va = A * m[0, 0] + B * m[0, 1] + C * m[0, 2];
            vb = A * m[1, 0] + B * m[1, 1] + C * m[1, 2];
            vc = A * m[2, 0] + B * m[2, 1] + C * m[2, 2];

            m_a = va;
            m_b = vb;
            m_c = vc;

            this.Normalizar();
        }

        public float DotProduct(Vector3D v)
        {
            return m_a * v.A + m_b * v.B + m_c * v.C;
        }

        public float DotProduct(Point3D pt)
        {
            return m_a * pt.X + m_b * pt.Y + m_c * pt.Z;
        }

        public Vector3D ProdutoVetorial(Vector3D v)
        {
            return new Vector3D((this.B * v.C - this.C * v.B), (this.C * v.A - this.A * v.C), (this.A * v.B - this.B * v.A));
        }

        public void IncrementarVetor(Vector3D v)
        {
            Vector3D vCross = v.ProdutoVetorial(Vector3D.VetorUp).Versor();
            Angle ang = v.Angulo(Vector3D.VetorUp);
            this.Rotacionar(vCross, -ang);
        }

        public void DecrementarVetor(Vector3D v)
        {
            Vector3D vCross = v.ProdutoVetorial(Vector3D.VetorUp).Versor();
            Angle ang = v.Angulo(Vector3D.VetorUp);
            this.Rotacionar(vCross, ang);
        }

        public void InverterABC(bool inverterA, bool inverterB, bool inverterC)
        {
            this.Setar(inverterA ? -this.A : this.A,
                inverterB ? -this.B : this.B,
                inverterC ? -this.C : this.C);
        }

        public static Angle Angulo(Vector3D u, Vector3D v)
        {
            if (u == v)
                return new Angle(AngleType.Cosseno, 1);

            float dividendo = u * v;
            float divisor = (u.Modulo() * v.Modulo());
            if (divisor == 0)
                return new Angle(AngleType.Cosseno, 0);
            else
                return new Angle(AngleType.Cosseno, dividendo / divisor);
        }

        public static Vector3D Normal(Point3D pt1, Point3D pt2, Point3D pt3)
        {
            /*
            Vetor3D u = new Vetor3D( pt1, pt2 ); 
            Vetor3D v = new Vetor3D( pt2, pt3 ); 
            return u.ProdutoVetorial(v).Versor(); 
            */

            return new Vector3D(pt1, pt2).ProdutoVetorial(new Vector3D(pt2, pt3)).Versor();
        }

        public static Vector3D operator +(Vector3D u, Vector3D v)
        {
            return new Vector3D(u.A + v.A, u.B + v.B, u.C + v.C);
        }

        public static Vector3D operator -(Vector3D u)
        {
            return new Vector3D(u.A, u.B, u.C);
        }

        public static float operator *(Vector3D u, Vector3D v)
        {
            return u.A * v.A + u.B * v.B + u.C * v.C;
        }

        public static Vector3D operator *(Vector3D vetor, float fator)
        {
            return new Vector3D(vetor.A * fator, vetor.B * fator, vetor.C * fator);
        }

        public static Vector3D operator /(Vector3D vetor, float fator)
        {
            return new Vector3D(vetor.A / fator, vetor.B / fator, vetor.C / fator);
        }

        public static bool operator ==(Vector3D v0, Vector3D v1)
        {
            if ((object)v0 == null)
                return ((object)v1 == null);

            if ((object)v1 == null)
                return false;

            Vector3D vet0 = v0.Clone() as Vector3D;
            vet0.Normalizar();

            Vector3D vet1 = v1.Clone() as Vector3D;
            vet1.Normalizar();

            if (!MathFuncs.IsClose(vet0.A, vet1.A))
                return false;
            if (!MathFuncs.IsClose(vet0.B, vet1.B))
                return false;
            if (!MathFuncs.IsClose(vet0.C, vet1.C))
                return false;

            return true;
        }
        public static bool operator !=(Vector3D v0, Vector3D v1)
        {
            return !(v0 == v1);
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
            //return this.A.GetHashCode( ) ^ this.B.GetHashCode( ) ^ this.C.GetHashCode( );
        }

        public override string ToString()
        {
            return string.Concat(Math.Round(m_a, 3), "; ", Math.Round(m_b, 3), "; ", Math.Round(m_c, 3));
        }

        public override bool Equals(object obj)
        {
            return (this == (obj as Vector3D));
        }

        #region ICloneable Members

        public object Clone()
        {
            return new Vector3D(m_a, m_b, m_c);
        }

        #endregion
    }
}