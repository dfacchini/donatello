﻿using System;
using Tao.OpenGl;
using System.Collections.Generic;
using openGl_Control.Math_Funcs;
using System.Resources;
using System.Reflection;
using System.IO;

namespace openGl_Control
{
    public class World
    {
        #region Properties

        private const int COMMOMZ = 10;

        public List<IDrawable> drawableObjects = null;
        public Tao.Platform.Windows.SimpleOpenGlControl viewControl = null;

        internal bool PickingMode { get; set; }

        private int[] MatrixView;

        private double[] MatrixModel;
        private double[] MatrixProjection;

        private float currentMouseX, currentMouseY;

        private int Width, Height;

        private float axeX, axeY, axeZ;

        private Point3D ptobs = new Point3D(0, 0, 100);
        private Point3D ptalvo = new Point3D(0, 0, 0);

        #endregion

        #region Constructor

        public World(List<IDrawable> drawableObjects)
        {
            if (drawableObjects == null)
                throw new ArgumentNullException("drawableObjects");

            MatrixModel = new double[16];
            MatrixProjection = new double[16];
            MatrixView = new int[4];
            this.drawableObjects = drawableObjects;
        }

        #endregion

        #region Internal Methods

        internal void InitGl()
        {
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            //Gl.glShadeModel(Gl.GL_FLAT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_CULL_FACE);
            Gl.glEnable(Gl.GL_NORMALIZE);
        }

        public static int Program;
        private int vertex, fragment;

        private void CreateProgram()
        {
            Program = Gl.glCreateProgram();

            Gl.glAttachShader(Program, vertex);
            Gl.glAttachShader(Program, fragment);

            Gl.glLinkProgram(Program);


            int linked;
            Gl.glGetProgramiv(Program, Gl.GL_LINK_STATUS, out linked);
            if (linked == 1)
            {
                // ok!
            }
        }

        private void InitShader()
        {
            CreateShader(out vertex, out fragment);
        }

        private void CreateShader(out int vertex, out int fragment)
        {
            int vertexLeng, fragmentLeng;
            string[] vertexStr, fragmentStr;

            this.CreateShaders(out vertex, out fragment);
            this.LoadShaderFiles(out vertexStr, out fragmentStr, out vertexLeng, out fragmentLeng);
            this.SetShaderSource(vertex, fragment, vertexStr, fragmentStr, vertexLeng, fragmentLeng);
            this.CompileShader(vertex, fragment);
        }

        private void CreateShaders(out int vertex, out int fragment)
        {
            vertex = Gl.glCreateShader(Gl.GL_VERTEX_SHADER);
            fragment = Gl.glCreateShader(Gl.GL_FRAGMENT_SHADER);
        }

        private void LoadShaderFiles(out string[] vertexStr, out string[] fragmentStr, out int vertexLeng, out int fragmentLeng )
        {
            vertexStr = new string[1];
            fragmentStr = new string[1];

            vertexStr[0] = openGl_Control.Properties.Resources.selection_vert;
            fragmentStr[0] = openGl_Control.Properties.Resources.selection_frag;

            vertexLeng = vertexStr[0].Length;
            fragmentLeng = fragmentStr[0].Length;
        }

        private void SetShaderSource(int vertex, int fragment, string[] vertexStr, string[] fragmentStr, int vertexLength, int fragmentLength)
        {
            Gl.glShaderSource( vertex, 1, vertexStr, ref vertexLength );
            Gl.glShaderSource( fragment, 1, fragmentStr, ref fragmentLength );
        }


        private void CompileShader(int vertex, int fragment)
        {
            Gl.glCompileShader(vertex);
            Gl.glCompileShader(fragment);
        }



        internal void InitPrimitives()
        {
            foreach (IDrawable drawableObject in this.drawableObjects)
            {
                drawableObject.InitializePrimitives();
                drawableObject.InitializePickPrimitives();
            }
        }

        internal void InitPickMode()
        {
            foreach (IDrawable drawableObject in this.drawableObjects)
            {
                drawableObject.InitializePickPrimitives();
            }

            this.PickingMode = true;

            /*
             * 
             * void MouseDownEvent(int x, int y)
 {
      // turn off texturing, lighting and fog
      glDisable(GL_TEXTURE_2D);
      glDisable(GL_FOG);
      glDisable(GL_LIGHTING);
 
      // render every object in our scene
      // suppose every object is stored in a list container called SceneObjects
      list<SceneObject *>::iterator itr = SceneObjects.begin();
      while(itr != SceneObjects.end())
      {
           (*itr)->Picking();
           itr++;
      }
 
      // get color information from frame buffer
      unsigned char pixel[3];
 
      GLint viewport[4];
      glGetIntegerv(GL_VIEWPORT, viewport);
 
      glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
 
      // now our picked screen pixel color is stored in pixel[3]
      // so we search through our object list looking for the object that was selected
      itr = SceneObjects.begin();
      while(itr != SceneObjects.end())
      {
           if((*itr)->m_colorID[0] == pixel[0] && (*itr)->m_colorID[1] == pixel[1] && (*itr)->m_colorID[2] == pixel[2])
           {
                // flag object as selected
                SetSelected((*itr);
                break;
           }
           itr++;
      }
 }
             */


        }

        internal void PickScene(int x, int y)
        {
            /*Proof = https://www.opengl.org/wiki/Common_Mistakes#Selection_and_Picking_and_Feedback_Mode
             *        https://www.opengl.org/discussion_boards/showthread.php/176080-What-is-the-best-method-for-object-picking
             *        https://www.opengl.org/archives/resources/faq/technical/selection.htm
             *        http://content.gpwiki.org/index.php/OpenGL_Selection_Using_Unique_Color_IDs
             *        
             *        http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=21
             *        https://www.opengl.org/sdk/docs/man2/xhtml/glReadPixels.xml
             */

            //Invert Y
            y = this.viewControl.Height - y;

            // turn off texturing, lighting and fog
            Gl.glDisable(Gl.GL_TEXTURE_2D);
            Gl.glDisable(Gl.GL_FOG);
            Gl.glDisable(Gl.GL_LIGHTING);

            //Gl.glReadBuffer(Gl.GL_BACK);
            this.RenderSceneToPick();

            // get color information from frame buffer
            byte[] pixel = new byte[3];

            int[] viewport = new int[4];
            Gl.glGetIntegerv(Gl.GL_VIEWPORT, viewport);

            Gl.glReadPixels(x, y, 1, 1, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, pixel);
            //Gl.glReadPixels(Int32.Parse(outValue.X.ToString()), Int32.Parse(outValue.Y.ToString()), 1, 1, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, pixel);

            Console.WriteLine("R " + pixel[0] + " G " + pixel[1] + " B " + pixel[2]);

            int index = ColorByIndex.GetIndexByColor(pixel[0], pixel[1], pixel[2]);

            foreach (IDrawable drawableObject in this.drawableObjects)
            {
                if (drawableObject.IndexColor == index)
                {
                    Console.WriteLine(drawableObject.GetType().ToString());
                }
            }

            this.RenderScene();

            // now our picked screen pixel color is stored in pixel[3]
            // so we search through our object list looking for the object that was selected
            //itr = SceneObjects.begin();
            //while(itr != SceneObjects.end())
            //{
            //     if((*itr)->m_colorID[0] == pixel[0] && (*itr)->m_colorID[1] == pixel[1] && (*itr)->m_colorID[2] == pixel[2])
            //     {
            //          // flag object as selected
            //          SetSelected((*itr);
            //          break;
            //     }
            //     itr++;
            //}
        }

        internal void RenderSceneToPick()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); //limpa buffer de cor e buffer de profundidade
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(ptobs.X, ptobs.Y, ptobs.Z, 0, 0, 0, 0f, 1f, 0f);
            //Glu.gluLookAt(0, 0, 150, 0, 0, 0, 0f, 1f, 0f);

            Gl.glPushMatrix();

            Gl.glTranslatef(0, 0, axeZ);

            Gl.glRotatef(axeX, 1, 0, 0);
            Gl.glRotatef(axeY, 0, 1, 0);

            Gl.glUseProgram(Program);
            foreach (IDrawable drawableObject in this.drawableObjects)
                drawableObject.DrawToPick();
            Gl.glUseProgram(0);
            
            Gl.glPopMatrix();
            Gl.glFlush();

            int error = Gl.glGetError();
            if (error != 0)
                throw new ApplicationException("An error has occurred: " + error.ToString());
        }

        internal void RenderScene()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); //limpa buffer de cor e buffer de profundidade
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            Glu.gluLookAt(ptobs.X, ptobs.Y, ptobs.Z, 0, 0, 0, 0f, 1f, 0f);
            //Glu.gluLookAt(0, 0, 150, 0, 0, 0, 0f, 1f, 0f);

            Gl.glPushMatrix();

            Gl.glTranslatef(0, 0, axeZ);

            Gl.glRotatef(axeX, 1, 0, 0);
            Gl.glRotatef(axeY, 0, 1, 0);

            foreach (IDrawable drawableObject in this.drawableObjects)
                drawableObject.Draw();
            Gl.glPopMatrix();

            this.viewControl.SwapBuffers();

            Gl.glFlush();

            int error = Gl.glGetError();
            if (error != 0)
                throw new ApplicationException("An error has occurred: " + error.ToString());
        }

        internal void SetView(int height, int width)
        {
            MatrixView[0] = 0;
            MatrixView[1] = 0;
            MatrixView[2] = width;
            MatrixView[3] = height;

            Width = width;
            Height = height;

            Gl.glViewport(0, 0, width, height);

            InitShader();
            CreateProgram();

            Gl.glEnable(Gl.GL_CULL_FACE);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();

            //Glu.gluPerspective(45.0f, (float)width / (float)height, 5f, 5000f);

            Glu.gluPerspective(45.0f, (float)width / (float)height, 5f, 500f);

            Gl.glEnable(Gl.GL_DEPTH_TEST);                                  //elimina faces escondidas

            Gl.glEnable(Gl.GL_MULTISAMPLE_ARB);                             // Bloco -> aumenta antiliasing, qualidades das linhas
            Gl.glEnable(Gl.GL_LINE_SMOOTH);                                 //
            Gl.glEnable(Gl.GL_POINT_SMOOTH);                                //
            Gl.glEnable(Gl.GL_BLEND);                                       //
            Gl.glBlendFunc(Gl.GL_SRC_ALPHA, Gl.GL_ONE_MINUS_SRC_ALPHA);     //
            Gl.glHint(Gl.GL_LINE_SMOOTH_HINT, Gl.GL_DONT_CARE);             //
            Gl.glLineWidth(1.5f);                                           //

            Gl.glShadeModel(Gl.GL_SMOOTH);
            Gl.glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            RenderScene();
        }

        private void UnProject(float x, float y, ref Point3D outPoint)
        {
            double[] mMatrixModel = new double[16];
            double[] mMatrixProjection = new double[16];

            Gl.glGetDoublev(Gl.GL_MODELVIEW_MATRIX, mMatrixModel);
            Gl.glGetDoublev(Gl.GL_PROJECTION_MATRIX, mMatrixProjection);
            Gl.glGetIntegerv(Gl.GL_VIEWPORT, MatrixView);

            double ptX, ptY, ptZ;
            Glu.gluUnProject((double)x, MatrixView[3] - (double)y, 0, mMatrixModel, mMatrixProjection, MatrixView, out ptX, out ptY, out ptZ);
            
            outPoint.X = (float)ptX;
            outPoint.Y = (float)ptY;
            outPoint.Z = (float)ptZ;
        }

        public Point3D ProjetaPontoNoAmbiente(float x, float y)
        {
            return ProjectPoint(x, y, true);
            //return m_sceneMgr.RenderSystem.ProjectPoint(x, y);
            /*float z;
            Gl.glReadPixels( ( int )x, ( int )(m_projetaView[3] - y), 1, 1, Gl.GL_DEPTH_COMPONENT, Gl.GL_FLOAT, out z );
            if (z == 1)
                    return null;	
            else
            {
                    ProjetaPonto(ref x, ref y, ref z); 
                    return new Ponto3D(x, y, z); 
            }*/
        }

        private Point3D ProjectPoint(float x, float y, bool ignoreZFarPlane)
        {
            float[] zWindow = new float[1];

            Gl.glReadPixels((int)x, (int)(MatrixView[3] - y), 1, 1, Gl.GL_DEPTH_COMPONENT, Gl.GL_FLOAT, zWindow);

            if (zWindow[0] == 1 && ignoreZFarPlane)
                return null;


            return ProjectPoint(x, y, zWindow[0]);
        }

        private Point3D ProjectPoint(float x, float y, float z)
        {
            double x2, y2, z2;

            Glu.gluUnProject(x, (double)(MatrixView[3] - y), z, GetMatrix(EMatrixMode.Model), GetMatrix(EMatrixMode.Projection), MatrixView, out x2, out y2, out z2);

            return new Point3D((float)x2, (float)y2, (float)z2);
        }

        private double[] GetMatrix(EMatrixMode mode)
        {
            switch (mode)
            {
                case EMatrixMode.Model:
                    Gl.glGetDoublev(Gl.GL_MODELVIEW_MATRIX, MatrixModel);
                    return MatrixModel;
                case EMatrixMode.Projection:
                    Gl.glGetDoublev(Gl.GL_PROJECTION_MATRIX, MatrixProjection);
                    return MatrixProjection;
                default:
                    return null;
            }
        }

        internal void UpdateZ(float wheel, float x, float y)
        {
            //Quando o zoom for < 0 (distanciando a camera) a camera volta no mesmo vetor de origem
            if (wheel > 0)
            {
                Point3D pt = new Point3D();
                UnProject(x, y, ref pt);
                currentMouseX = pt.X;
                currentMouseY = pt.Y;
            }

            if (wheel > 0)
                axeZ += COMMOMZ;
            else
                axeZ -= COMMOMZ;

            //foreach (IDrawable drawableObject in this.drawableObjects)
            //    if (drawableObject != null)
            //        drawableObject.UpdateZ(wheel);
        }

        internal void UpdateLocations(float x, float y)
        {
            axeX = x;
            axeY = y;
            //foreach (IDrawable drawableObject in this.drawableObjects)
            //    if (drawableObject != null)
            //        drawableObject.Update(x, y);
        }

        internal Point3D UpdateLocationX(float degrees)
        {
            double cDegrees = (Math.PI * degrees) / 180.0f; //Convert degrees to radian for .Net Cos/Sin functions
            double cosDegrees = Math.Cos(cDegrees);
            double sinDegrees = Math.Sin(cDegrees);

            double y = (ptobs.Y * cosDegrees) + (ptobs.Z * sinDegrees);
            double z = (ptobs.Y * -sinDegrees) + (ptobs.Z * cosDegrees);

            Point2D pt = new Point2D((float)y, (float)z);
            var a = Angle.EntreDoisPontos(new Point2D(0, 0), pt);

            ptobs.Y = pt.X;
            ptobs.Z = pt.Y;

            return ptobs;
        }

        internal Point3D UpdateLocationY(float degrees)
        {
            double cDegrees = (Math.PI * degrees) / 180.0; //Radians
            double cosDegrees = Math.Cos(cDegrees);
            double sinDegrees = Math.Sin(cDegrees);

            double x = (ptobs.X * cosDegrees) + (ptobs.Z * sinDegrees);
            double z = (ptobs.X * -sinDegrees) + (ptobs.Z * cosDegrees);

            Point2D pt = new Point2D((float)x, (float)z);

            ptobs.X = pt.X;
            ptobs.Z = pt.Y;

            return ptobs;
        }

        #endregion

        public void Rotate(float dx, float dy, Point2D ptScreen)
        {
            //https://www.youtube.com/watch?v=zc8b2Jo7mno 
            //http://blog.preoccupiedgames.com/quaternions-not-satan/
            Point3D ptProjecao = this.ProjectPoint(ptScreen.X, ptScreen.Y, true);

            //Ponto3D ptalvo = this.ViewInterfaceEstatica.PtVista.PtAlvo;
            //Ponto3D ptobs = this.ViewInterfaceEstatica.PtVista.PtObservador;
            //Ponto3D ptup = this.ViewInterfaceEstatica.PtVista.PtUp;

            if (Math.Abs(dx) > Math.Abs(dy))
            {
                {
                    float distXY = ptalvo.Distancia(new Point2D(ptobs.X, ptobs.Y));
                    Angle angXY = new Angle((360f / this.Width) * -dx);
                    if (ptProjecao != null)
                    {
                        float distProjObs = ptProjecao.Distancia(new Point2D(ptobs.X, ptobs.Y));
                        Angle angObsAlvo = angXY + Angle.EntreDoisPontos(ptobs.X, ptobs.Y, ptalvo.X, ptalvo.Y);
                        Angle angProjObs = angXY + Angle.EntreDoisPontos(ptProjecao.X, ptProjecao.Y, ptobs.X, ptobs.Y);

                        ptobs.X = ptProjecao.X + (float)Math.Cos(angProjObs.Radianos) * distProjObs;
                        ptobs.Y = ptProjecao.Y + (float)Math.Sin(angProjObs.Radianos) * distProjObs;

                        ptalvo.X = ptobs.X + (float)Math.Cos(angObsAlvo.Radianos) * distXY;
                        ptalvo.Y = ptobs.Y + (float)Math.Sin(angObsAlvo.Radianos) * distXY;
                    }
                    else
                    {
                        angXY = angXY + Angle.EntreDoisPontos(ptalvo.X, ptalvo.Y, ptobs.X, ptobs.Y);
                        ptobs.X = ptalvo.X + (float)Math.Cos(angXY.Radianos) * distXY;
                        ptobs.Y = ptalvo.Y + (float)Math.Sin(angXY.Radianos) * distXY;
                    }
                }
            }
            else
            {
                float distXYZ = ptalvo.Distancia(ptobs);
                double angInclinacao = 0;
                double angPlano = 0;
                double ang = 0;

                Vector3D v = new Vector3D(ptalvo, ptobs);
                angInclinacao = v.AnguloInclinacao.Degradianos;
                angPlano = v.AnguloPlano.Degradianos;


                ang = (180f / this.Height) * dy;

                if ((ptProjecao != null))
                {
                    Position pos = new Position(ptalvo);
                    pos.Normal.Setar(90 + Angle.EntreDoisPontos(ptalvo, ptobs), 90);
                    //pos.Normal.AnguloPlano = 90 + Angulo.EntreDoisPontos(ptalvo, ptobs);
                    //pos.Normal.AnguloInclinacao = 90;
                    Point3D pt = new Point3D();
                    if (pos.IntersectAparente(pt, ptProjecao))
                    {
                        ptalvo.Rotacionar(pos.Normal, new Angle(ang), pt);
                        ptobs.Rotacionar(pos.Normal, new Angle(ang), pt);
                    }
                }
                else
                {
                    angInclinacao += ang;
                    if (angInclinacao < 0)
                        angInclinacao = 0;
                    else if (angInclinacao > 170)
                        angInclinacao = 170;

                    v = new Vector3D(angPlano, angInclinacao);

                    ptobs.X = ptalvo.X + v.A * distXYZ;
                    ptobs.Y = ptalvo.Y + v.B * distXYZ;
                    ptobs.Z = ptalvo.Z + v.C * distXYZ;
                }
            }

            //AjustarZFar();

            //this.ViewInterfaceEstatica.PtVista.Resume();

            //this.ViewInterfaceEstatica.AplicarVista();
            //			this.ViewInterfaceDinamica.DrawZoom();
        }
    }
}
