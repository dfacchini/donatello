﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Math_Funcs
{
    public static class ColorByIndex
    {
        public static int GetIndexByColor(int r, int g, int b)
        {
            return (r) | (g << 8) | (b << 16);
        }

        public static float[] GetColorByIndex(int index)
        {
            int r = index & 0xFF;
            int g = (index >> 8) & 0xFF;
            int b = (index >> 16) & 0xFF;

            return new float[] { r / 255.0f, g / 255.0f, b / 255.0f };
        }
    }
}
