﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Math_Funcs
{
    public static class MathFuncs
    {
        public static double Deg(double rad)
        {
            return rad * 180d / Math.PI;
        }

        public static double Rad(double deg)
        {
            return deg * Math.PI / 180d;
        }

        public static bool IsClose(this double currentValue, double value, double precision = 0.00001)
        {
            return Math.Abs(currentValue - value) <= precision;
        }
    }
}
