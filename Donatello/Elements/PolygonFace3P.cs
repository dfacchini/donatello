﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class PolygonFace3P : PolygonBaseAbstract
    {

        //private const string FILEPOLYGON = "openGl_Control.Models.floorWall.1.plg";
        private const string FILEPOLYGON = "openGl_Control.Models.floorWall.1.plg";        
        //private const string FILEPOLYGON = "openGl_Control.Models.floorWW.plg";

        private const string FILEPOLYGONWIREFRAME = "openGl_Control.Models.floorWall.plg";
        private const int COMMOMZ = 3;

        private int List, ListPicking, ListShape, ListShapeWireframe;
        private float axeX, axeY, axeZ;

        private readonly List<Point3D> PolygonWireframe;
        private readonly List<Face3P> PolygonFaces;

        public PolygonFace3P()
        {
            PolygonWireframe = ReadPoints3D(FILEPOLYGONWIREFRAME);
            PolygonFaces = ReadFace3P(FILEPOLYGON);

            this.Centralize3D(PolygonWireframe);
            this.Centralize3D(PolygonFaces);
        }

        public override void Draw()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);

            Gl.glCallList(List);
            Gl.glPopMatrix();
        }

        public override void DrawToPick()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);

            Gl.glCallList(ListPicking);
            Gl.glPopMatrix();
        }

        public override void InitializePickPrimitives()
        {
            ListPicking = Gl.glGenLists(1);
            Gl.glNewList(ListPicking, Gl.GL_COMPILE);

            Gl.glColor3f(Color[0], Color[1], Color[2]);
            Gl.glCallList(ListShape);

            Gl.glEndList();
        }

        public override void InitializePrimitives()
        {
            InitializeShape();
            InitializeShapeWireFrame();

            List = Gl.glGenLists(1);
            Gl.glNewList(List, Gl.GL_COMPILE);


            //Gl.glColor3f(1f, 1f, 1f);
            //Gl.glCallList(ListShape);

            Gl.glColor3f(0.7f, 1f, 1f);
            Gl.glCallList(ListShape);

            Gl.glColor3f(0.1f, 0.1f, 0.1f);
            Gl.glCallList(ListShapeWireframe);

            Gl.glEndList();
        }

        private void InitializeShape()
        {
            ListShape = Gl.glGenLists(1);
            Gl.glNewList(ListShape, Gl.GL_COMPILE);

            Gl.glBegin(Gl.GL_TRIANGLES);
            foreach (var face in this.PolygonFaces)
            {
                DrawFace(face);
            }
            Gl.glEnd();

            Gl.glEndList();
        }

        private void DrawFace(Face3P face)
        {
            Gl.glNormal3dv(face.NormalVector);
            Gl.glEdgeFlag(1);

            Gl.glVertex3f(face.P3.X, face.P3.Z, face.P3.Y);

            Gl.glNormal3dv(face.NormalVector);
            Gl.glEdgeFlag(1);

            Gl.glVertex3f(face.P2.X, face.P2.Z, face.P2.Y);

            Gl.glNormal3dv(face.NormalVector);
            Gl.glEdgeFlag(1);

            Gl.glVertex3f(face.P1.X, face.P1.Z, face.P1.Y);
        }

        private void InitializeShapeWireFrame()
        {
            ListShapeWireframe = Gl.glGenLists(1);
            Gl.glNewList(ListShapeWireframe, Gl.GL_COMPILE);

            Gl.glLineWidth(2);
            //Gl.glBegin(Gl.GL_LINES);
            //foreach (var point in this.PolygonWireframe)
            //{
            //    Gl.glVertex3f(point.X, point.Z, point.Y);
            //}
            //Gl.glEnd();


            Gl.glBegin(Gl.GL_LINES);
            for (int i = 0; i < this.PolygonWireframe.Count; i += 2)
            {
                Gl.glVertex3f(PolygonWireframe[i].X, PolygonWireframe[i].Z, PolygonWireframe[i].Y);
                Gl.glVertex3f(PolygonWireframe[i + 1].X, PolygonWireframe[i + 1].Z, PolygonWireframe[i + 1].Y);
            }
            Gl.glEnd();


            Gl.glLineWidth(1.5f);

            Gl.glEndList();
        }


        public override void Update(float x, float y)
        {
            this.axeX = x;
            this.axeY = y;
        }

        public override void UpdateZ(float wheel)
        {
            if (wheel > 0)
                axeZ += COMMOMZ;
            else
                axeZ -= COMMOMZ;
        }

        public override void Translate(float x, float y, float z)
        {
        }
    }
}
