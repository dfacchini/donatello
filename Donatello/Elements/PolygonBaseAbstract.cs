﻿using openGl_Control.Math_Funcs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public abstract class PolygonBaseAbstract : IDrawable
    {
        private const int COMMOMD = 200;

        private float[] _color;
        public float[] Color
        {
            get
            {
                return _color;
            }
            private set
            {
                _color = value;
            }
        }

        private int _indexColor;
        public int IndexColor
        {
            get
            {
                return _indexColor;
            }
            set
            {
                _indexColor = value;
                UpdateColor(_indexColor);
            }
        }

        public abstract void Draw();

        public abstract void DrawToPick();

        public abstract void InitializePrimitives();

        public abstract void InitializePickPrimitives();

        public abstract void Update(float x, float y);

        public abstract void UpdateZ(float wheel);

        public abstract void Translate(float x, float y, float z);

        private void UpdateColor(int index)
        {
            Color = ColorByIndex.GetColorByIndex(index);
        }

        public List<Point3D> ReadPoints3D(string file, bool centralizeShape = false)
        {
            List<Point3D> points = new List<Point3D>();
            using (StreamReader streamReader = new StreamReader(this.GetType().Module.Assembly.GetManifestResourceStream(file)))
            {
                while (!streamReader.EndOfStream)
                {
                    String line = streamReader.ReadLine();
                    if (string.IsNullOrEmpty(line)) continue;

                    points.Add(new Point3D(line.Split(';')));
                }
            }

            return points;
        }

        public List<Face3P> ReadFace3P(string file, bool centralizeShape = false)
        {
            List<Face3P> faces = new List<Face3P>();
            using (StreamReader streamReader = new StreamReader(this.GetType().Module.Assembly.GetManifestResourceStream(file)))
            {
                while (!streamReader.EndOfStream)
                {
                    Point3D[] points = new Point3D[3];

                    for (int i = 0; i < 3; i++)
                    {
                        String line = streamReader.ReadLine();
                        if (string.IsNullOrEmpty(line)) continue;
                        points[i] = new Point3D(line.Split(';'));
                    }

                    String normalLine = streamReader.ReadLine();
                    if (string.IsNullOrEmpty(normalLine) || !normalLine.StartsWith("N ")) continue;
                    Vector3D normal = new Vector3D(new Point3D(normalLine.Substring(2).Split(';')));

                    faces.Add(new Face3P()
                    {
                        P1 = points[0],
                        P2 = points[1],
                        P3 = points[2],
                        Normal = normal,
                        NormalVector = new double[] { normal.A, normal.B, normal.C }
                    });
                }
            }

            return faces;
        }

        public Dimension GetDimension(List<Point3D> points)
        {
            var max = GetMaxPoints(points);
            var min = GetMinPoints(points);

            var width = Math.Abs(min.X - max.X);
            var height = Math.Abs(min.Y - max.Y);
            var depth = Math.Abs(min.Z - max.Z);

            return new Dimension(width, height, depth);
        }

        public void Centralize3D(List<Point3D> points, bool withProportion = true)
        {
            float biggerX, smallerX, biggerY, smallerY, biggerZ, smallerZ;

            if (withProportion)
            {
                biggerX = points.Max(i => i.X) / COMMOMD;
                smallerX = points.Min(i => i.X) / COMMOMD;

                biggerY = points.Max(i => i.Y) / COMMOMD;
                smallerY = points.Min(i => i.Y) / COMMOMD;

                biggerZ = points.Max(i => i.Z) / COMMOMD;
                smallerZ = points.Min(i => i.Z) / COMMOMD;

                points.ForEach(i =>
                {
                    i.X /= COMMOMD;
                    i.Y /= COMMOMD;
                    i.Z /= COMMOMD;
                });
            }
            else
            {
                biggerX = points.Max(i => i.X);
                smallerX = points.Min(i => i.X);

                biggerY = points.Max(i => i.Y);
                smallerY = points.Min(i => i.Y);

                biggerZ = points.Max(i => i.Z);
                smallerZ = points.Min(i => i.Z);
            }

            points.ForEach(i =>
            {
                i.X += -(biggerX + smallerX) / 2.0f;
                i.Y += -(biggerY + smallerY) / 2.0f;
                i.Z += -(biggerZ + smallerZ) / 2.0f;
            });
        }

        private Point3D GetMaxPoints(List<Point3D> points)
        {
            float biggerX, biggerY, biggerZ;

            biggerX = points.Max(i => i.X);
            biggerY = points.Max(i => i.Y);
            biggerZ = points.Max(i => i.Z);

            return new Point3D(biggerX, biggerY, biggerZ);
        }

        private Point3D GetMinPoints(List<Point3D> points)
        {
            float smallerX, smallerY, smallerZ;

            smallerX = points.Min(i => i.X);
            smallerY = points.Min(i => i.Y);
            smallerZ = points.Min(i => i.Z);

            return new Point3D(smallerX, smallerY, smallerZ);
        }

        private Func<List<Face3P>, MinMaxStruct> GetMinMaxFace3P = new Func<List<Face3P>, MinMaxStruct>((points) =>
        {
            MinMaxStruct structReturn = new MinMaxStruct();

            float[] maxValues = new float[] { points.Max(i => i.P1.X), points.Max(i => i.P2.X), points.Max(i => i.P3.X) };
            float[] minValues = new float[] { points.Min(i => i.P1.X), points.Min(i => i.P2.X), points.Min(i => i.P3.X) };

            structReturn.MaxX = maxValues.Max();
            structReturn.MinX = minValues.Min();

            maxValues = new float[] { points.Max(i => i.P1.Y), points.Max(i => i.P2.Y), points.Max(i => i.P3.Y) };
            minValues = new float[] { points.Min(i => i.P1.Y), points.Min(i => i.P2.Y), points.Min(i => i.P3.Y) };

            structReturn.MaxY = maxValues.Max();
            structReturn.MinY = minValues.Min();

            maxValues = new float[] { points.Max(i => i.P1.Z), points.Max(i => i.P2.Z), points.Max(i => i.P3.Z) };
            minValues = new float[] { points.Min(i => i.P1.Z), points.Min(i => i.P2.Z), points.Min(i => i.P3.Z) };

            structReturn.MaxZ = maxValues.Max();
            structReturn.MinZ = minValues.Min();

            return structReturn;
        });

        public void Centralize3D(List<Face3P> points, bool withProportion = true)
        {
            MinMaxStruct centralizeStruct = GetMinMaxFace3P(points); ;
            if (withProportion)
            {
                centralizeStruct.MaxX /= COMMOMD;
                centralizeStruct.MinX /= COMMOMD;

                centralizeStruct.MaxY /= COMMOMD;
                centralizeStruct.MinY /= COMMOMD;

                centralizeStruct.MaxZ /= COMMOMD;
                centralizeStruct.MinZ /= COMMOMD;

                var dividePt = new Action<Point3D>((p) => { p.X /= COMMOMD; p.Y /= COMMOMD; p.Z /= COMMOMD; });

                points.ForEach(i => { dividePt(i.P1); dividePt(i.P2); dividePt(i.P3); });
            }

            points.ForEach(i =>
            {
                i.P1.X += -(centralizeStruct.MaxX + centralizeStruct.MinX) / 2.0f;
                i.P2.X += -(centralizeStruct.MaxX + centralizeStruct.MinX) / 2.0f;
                i.P3.X += -(centralizeStruct.MaxX + centralizeStruct.MinX) / 2.0f;

                i.P1.Y += -(centralizeStruct.MaxY + centralizeStruct.MinY) / 2.0f;
                i.P2.Y += -(centralizeStruct.MaxY + centralizeStruct.MinY) / 2.0f;
                i.P3.Y += -(centralizeStruct.MaxY + centralizeStruct.MinY) / 2.0f;

                i.P1.Z += -(centralizeStruct.MaxZ + centralizeStruct.MinZ) / 2.0f;
                i.P2.Z += -(centralizeStruct.MaxZ + centralizeStruct.MinZ) / 2.0f;
                i.P3.Z += -(centralizeStruct.MaxZ + centralizeStruct.MinZ) / 2.0f;
            });
        }

        public void SetUniform(string name, float[] value)
        {
            //Gl.glUniform3f(World.Program, Color[0], Color[1], Color[2]);
            int rgbaLocation = Gl.glGetUniformLocation(World.Program, name);
            Gl.glUniform4f(rgbaLocation, Color[0]
                                        , Color[1]
                                        , Color[2], 1f);

            //Gl.glUniform4f(rgbaLocation, 1f, 0f, 0f, 1f);

        }
    }
}
