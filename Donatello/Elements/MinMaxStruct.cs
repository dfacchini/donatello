﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace openGl_Control.Elements
{
    public struct MinMaxStruct
    {
        public float MinX { get; set; }
        public float MaxX { get; set; }

        public float MinY { get; set; }
        public float MaxY { get; set; }

        public float MinZ { get; set; }
        public float MaxZ { get; set; }
    }
}
