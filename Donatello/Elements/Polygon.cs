﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Polygon : PolygonBaseAbstract
    {
        private const string FILEPOLYGON = "openGl_Control.Models.polygon2.2.plg";
        private const string FILEPOLYGONLINES = "openGl_Control.Models.polygon2.3.plg";
        private const int COMMOMZ = 3;

        private int List, ListPicking, ListShape, ListWireframe;
        private float axeX, axeY, axeZ;

        public float TranslateX, TranslteY, TranslteZ;

        private readonly List<Point3D> PolygonPoints;
        private readonly List<Point3D> PolygonLinesPoints;

        public Polygon()
        {
            PolygonPoints = ReadPoints3D(FILEPOLYGON);
            PolygonLinesPoints = ReadPoints3D(FILEPOLYGONLINES);

            this.Centralize3D(PolygonPoints);
            this.Centralize3D(PolygonLinesPoints);


            var dimension = this.GetDimension(PolygonLinesPoints);
        }

        public override void Draw()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);

            //SetUniform("rgba", new float[] { Color[0], Color[1], Color[2], 1 });

            Gl.glCallList(List);
            //Gl.glCallList(ListPicking);
            Gl.glPopMatrix();
        }
        
        public override void DrawToPick()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);
            Gl.glGetString(Gl.GL_VENDOR);
            SetUniform("rgba", new float[] { Color[0], Color[1], Color[2], 1 });

            Gl.glCallList(ListPicking);
            Gl.glPopMatrix();
        }

        int vboID;

        public override void InitializePickPrimitives()
        {
            ListPicking = Gl.glGenLists(1);
            Gl.glNewList(ListPicking, Gl.GL_COMPILE);

            Gl.glColor3f(Color[0], Color[1], Color[2]);
            
            Gl.glCallList(ListShape);            
            //Gl.glCallList(ListWireframe);

            Gl.glEndList();
        }

        public override void InitializePrimitives()
        {
            InitializeShape();
            //InitializeWireframe();

            List = Gl.glGenLists(1);
            Gl.glNewList(List, Gl.GL_COMPILE);

            Gl.glColor3f(0.5f, 0.5f, 0.3f);
            //Gl.glColor3f(Color[0], Color[1], Color[2]);
            Gl.glCallList(ListShape);

            //Gl.glColor3f(0.2f, 0.5f, 0.7f);
            //Gl.glCallList(ListWireframe);

            Gl.glEndList();
        }

        private void InitializeShape()
        {
            ListShape = Gl.glGenLists(1);
            Gl.glNewList(ListShape, Gl.GL_COMPILE);

            Gl.glTranslatef(TranslateX, TranslteY, TranslteZ);

            Gl.glColor3f(1f, 1f, 0f);
            Gl.glBegin(Gl.GL_QUADS);
            foreach (var point in this.PolygonPoints)
            {
                Gl.glVertex3f(point.X, point.Y, point.Z);
            }
            Gl.glEnd();

            Gl.glEndList();
        }

        private void InitializeWireframe()
        {
            ListWireframe = Gl.glGenLists(1);
            Gl.glNewList(ListWireframe, Gl.GL_COMPILE);

            Gl.glTranslatef(TranslateX, TranslteY, TranslteZ);

            Gl.glLineWidth(3);
            Gl.glBegin(Gl.GL_LINES);
            foreach (var point in this.PolygonLinesPoints)
            {
                Gl.glVertex3f(point.X, point.Y, point.Z);
            }
            Gl.glEnd();
            Gl.glLineWidth(1.5f);

            Gl.glEndList();
        }

        public override void Update(float x, float y)
        {
            this.axeX = x;
            this.axeY = y;
        }

        public override void UpdateZ(float wheel)
        {
            if (wheel > 0)
                axeZ += COMMOMZ;
            else
                axeZ -= COMMOMZ;
        }

        public override void Translate(float x, float y, float z)
        {
        }
    }
}
