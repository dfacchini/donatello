﻿using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Axes : PolygonBaseAbstract
    {
        private const float AxisSize = 100.0f;

        private int List, ListPicking, ListAxe;

        private float axesXLocation;
        private float axesYLocation;
        private float axeZ;

        public override void Draw()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);
            Gl.glRotatef(this.axesXLocation, 1f, 0f, 0f);
            Gl.glRotatef(this.axesYLocation, 0f, 1f, 0f);

            Gl.glCallList(List);

            Gl.glPopMatrix();
        }

        public override void DrawToPick()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);
            Gl.glRotatef(this.axesXLocation, 1f, 0f, 0f);
            Gl.glRotatef(this.axesYLocation, 0f, 1f, 0f);

            SetUniform("rgba", new float[] { Color[0], Color[1], Color[2], 1 });

            Gl.glCallList(ListPicking);

            Gl.glPopMatrix();
        }

        public override void Update(float x, float y)
        {
            this.axesXLocation = x;
            this.axesYLocation = y;
        }

        public override void InitializePrimitives()
        {
            InitializeAxes();

            List = Gl.glGenLists(1);
            Gl.glNewList(List, Gl.GL_COMPILE);

            Gl.glColor3f(1.0f, 0.0f, 0.0f);
            Gl.glCallList(ListAxe);

            Gl.glEndList();
        }

        public override void InitializePickPrimitives()
        {
            ListPicking = Gl.glGenLists(1);
            Gl.glNewList(ListPicking, Gl.GL_COMPILE);

            Gl.glColor3f(Color[0], Color[1], Color[2]);
            Gl.glCallList(ListAxe);

            Gl.glEndList();
        }

        private void InitializeAxes()
        {
            ListAxe = Gl.glGenLists(1);
            Gl.glNewList(ListAxe, Gl.GL_COMPILE);

            Gl.glLineWidth(2);
            // draw a line along the z-axis //
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(0.0f, 0.0f, -AxisSize);
            Gl.glVertex3f(0.0f, 0.0f, AxisSize);
            Gl.glEnd();

            // draw a line along the y-axis //
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(0.0f, -AxisSize, 0.0f);
            Gl.glVertex3f(0.0f, AxisSize, 0.0f);
            Gl.glEnd();

            // draw a line along the x-axis //
            Gl.glBegin(Gl.GL_LINES);
            Gl.glVertex3f(-AxisSize, 0.0f, 0.0f);
            Gl.glVertex3f(AxisSize, 0.0f, 0.0f);
            Gl.glEnd();
            Gl.glLineWidth(1.5f);

            Gl.glEndList();
        }

        public override void UpdateZ(float wheel)
        {
            if (wheel > 0)
                axeZ += 3f;
            else
                axeZ -= 3f;
        }

        public override void Translate(float x, float y, float z)
        {
        }
    }
}
