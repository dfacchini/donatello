﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tao.OpenGl;

namespace openGl_Control.Elements
{
    public class Floor : PolygonBaseAbstract
    {
        private const string FILEFLOOR = "openGl_Control.Models.floor.plg";
        private const int COMMOMZ = 3;

        private int List, ListPicking, ListFloor;
        private float axeX, axeY, axeZ;

        private readonly List<Point3D> FloorPoints = new List<Point3D>();

        public Floor()
        {
            this.FloorPoints = ReadPoints3D(FILEFLOOR);
        }

        public override void Draw()
        {            
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);

            Gl.glCallList(List);

            Gl.glPopMatrix();
        }

        public override void DrawToPick()
        {
            Gl.glPushMatrix();

            Gl.glTranslatef(0.0f, 0.0f, axeZ);

            Gl.glRotatef(this.axeX, 1f, 0f, 0f);
            Gl.glRotatef(this.axeY, 0f, 1f, 0f);

            SetUniform("rgba", new float[] { Color[0], Color[1], Color[2], 1 });

            Gl.glCallList(ListPicking);

            Gl.glPopMatrix();
        }

        public override void InitializePrimitives()
        {
            InitializeFloor();

            List = Gl.glGenLists(1);
            Gl.glNewList(List, Gl.GL_COMPILE);

            Gl.glColor3f(0.5f, 0.5f, 0.5f);
            Gl.glCallList(ListFloor);

            Gl.glEndList();
        }

        public override void InitializePickPrimitives()
        {
            ListPicking = Gl.glGenLists(1);
            Gl.glNewList(ListPicking, Gl.GL_COMPILE);

            Gl.glColor3f(Color[0], Color[1], Color[2]);
            Gl.glCallList(ListFloor);

            Gl.glEndList();
        }

        private void InitializeFloor()
        {
            ListFloor = Gl.glGenLists(1);
            Gl.glNewList(ListFloor, Gl.GL_COMPILE);

            Gl.glBegin(Gl.GL_LINES);
            foreach (Point3D line in this.FloorPoints)
            {
                Gl.glVertex3f(line.X, line.Y, line.Z);
            }
            Gl.glEnd();

            Gl.glEndList();
        }

        public override void Update(float x, float y)
        {
            this.axeY = y;
            this.axeX = x;
        }

        public override void UpdateZ(float wheel)
        {
            if (wheel > 0)
                axeZ += COMMOMZ;
            else
                axeZ -= COMMOMZ;
        }

        public override void Translate(float x, float y, float z)
        {
        }
    }
}
