﻿
namespace openGl_Control
{
    public interface IDrawable
    {
        float[] Color { get; }
        int IndexColor { get; set; }

        void Draw();
        void DrawToPick();
        void InitializePrimitives();

        void InitializePickPrimitives();

        void Update(float x, float y);
        void UpdateZ(float wheel);

        void Translate(float x, float y, float z);
    }
}
