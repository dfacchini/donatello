using openGl_Control.Math_Funcs;
using System;
using System.ComponentModel;

namespace openGl_Control
{

    public class Plan
    {
        public static float TOLERANCIA_DEGENERACAO = 0.00001f;

        private Vector3D m_normal;
        private Point3D m_pontoBase;

        public Plan(Vector3D normal, Point3D pontoBase)
        {
            m_normal = normal;
            m_pontoBase = pontoBase;
        }

        public Plan()
            : this(new Vector3D(0, 0, 1), new Point3D(0, 0, 0))
        {
        }

        public Plan(float A, float B, float C, float X, float Y, float Z)
            : this(new Vector3D(A, B, C), new Point3D(X, Y, Z))
        {
        }

        public Plan(float A, float B, float C, Point3D pontoBase)
            : this(new Vector3D(A, B, C), pontoBase)
        {
        }

        public Plan(Vector3D normal, float X, float Y, float Z)
            : this(normal, new Point3D(X, Y, Z))
        {
        }

        public bool IntersectReal(Point3D inters, Point3D p1, Point3D p2)
        {
            if (IntersectAparente(inters, p1, p2))
            {
                return (
                    CoordenadaEntrePontos(p1.X, inters.X, p2.X)
                    && CoordenadaEntrePontos(p1.Y, inters.Y, p2.Y)
                    && CoordenadaEntrePontos(p1.Z, inters.Z, p2.Z)
                    );
            }
            else
                return false;
        }

        private bool CoordenadaEntrePontos(float ci, float cm, float cf)
        {
            if (ci < cf)
            {
                //return ((cm>=ci) && (cm<=cf));
                return ((cm > ci || MathFuncs.IsClose(cm, ci, 0.01d)) &&
                         (cm < cf || MathFuncs.IsClose(cm, cf, 0.01d)));
            }
            else
            {
                //return ((cm<=ci) && (cm>=cf));
                return ((cm < ci || MathFuncs.IsClose(cm, ci, 0.01d)) &&
                         (cm > cf || MathFuncs.IsClose(cm, cf, 0.01d)));
            }
        }

        public bool IntersectAparente(Point3D inters, Point3D p1, Point3D p2)
        {
            double deno = (p1.X - p2.X) * m_normal.A;
            deno += (p1.Y - p2.Y) * m_normal.B;
            deno += (p1.Z - p2.Z) * m_normal.C;

            //if (deno == 0) 
            if (MathFuncs.IsClose(deno, 0))
                return false;
            else
            {
                double numer = (m_pontoBase.X - p2.X) * m_normal.A;
                numer += (m_pontoBase.Y - p2.Y) * m_normal.B;
                numer += (m_pontoBase.Z - p2.Z) * m_normal.C;

                double T = numer / deno;
                double omt = 1 - T;

                if ((1 < T * TOLERANCIA_DEGENERACAO) || (-1 > T * TOLERANCIA_DEGENERACAO))
                    return false;
                inters.X = (float)T * p1.X + (float)omt * p2.X;
                inters.Y = (float)T * p1.Y + (float)omt * p2.Y;
                inters.Z = (float)T * p1.Z + (float)omt * p2.Z;
                return true;
            }
        }

        public bool IntersectAparente(Point3D inters, Point3D p1)
        {
            Point3D p2 = new Point3D(p1.X + m_normal.A * 1, p1.Y + m_normal.B * 1, p1.Z + m_normal.C * 1);
            return IntersectAparente(inters, p1, p2);
        }
                
        public Point3D PontoBase
        {
            get
            {
                return m_pontoBase;
            }
            set
            {
                m_pontoBase = value;
            }
        }

        public Vector3D Normal
        {
            get
            {
                return m_normal;
            }
            set
            {
                m_normal = value;
            }
        }

        public Vector3D DirecaoPerpendicularNormal()
        {
            this.Normal.Normalizar();
            float a = (float)(this.Normal.AnguloInclinacao.Cosseno * this.Normal.AnguloPlano.Cosseno);
            float b = (float)(this.Normal.AnguloInclinacao.Cosseno * this.Normal.AnguloPlano.Seno);
            float c = (float)-(this.Normal.AnguloInclinacao.Seno);
            Vector3D v = new Vector3D(a, b, c);
            v.Normalizar();
            return v;
        }

        public Vector3D DirecaoPerpendicularNormal(Angle rotacao)
        {
            Vector3D v = this.DirecaoPerpendicularNormal();
            Point3D p = new Point3D(v.A, v.B, v.C);
            p.Rotacionar(this.Normal, rotacao);
            Vector3D u = new Vector3D(p.X, p.Y, p.Z);
            u.Normalizar();
            return u;
        }       
    }
}
