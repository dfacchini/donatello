using openGl_Control;
using openGl_Control.Math_Funcs;
using System;
using System.Globalization;

namespace openGl_Control
{
    public class Point3D
    {
        private float m_x, m_y, m_z;

        static Point3D()
        {
        }

        public Point3D()
        {
            m_x = m_y = m_z = 0F;
        }

        public Point3D(Point2D ponto)
        {
            m_x = ponto.X;
            m_y = ponto.Y;
            m_z = 0F;
        }

        public Point3D(string[] points)
        {
            this.X = float.Parse(points[0], CultureInfo.InvariantCulture);
            this.Y = float.Parse(points[1], CultureInfo.InvariantCulture);
            this.Z = float.Parse(points[2], CultureInfo.InvariantCulture);
        }

        public Point3D(float x, float y, float z)
        {
            m_x = x;
            m_y = y;
            m_z = z;
        }

        public float X
        {
            get
            {
                return m_x;
            }
            set
            {
                m_x = value;
                if (float.IsNaN(m_x))
                    m_x = 0;
            }
        }

        public float Y
        {
            get
            {
                return m_y;
            }
            set
            {
                m_y = value;
                if (float.IsNaN(m_y))
                    m_y = 0;
            }
        }

        public float Z
        {
            get
            {
                return m_z;
            }
            set
            {
                m_z = value;
                if (float.IsNaN(m_z))
                    m_z = 0;
            }
        }


        public static bool operator ==(Point3D ponto1, object objeto)
        {
            return (((object)ponto1) == objeto);
        }

        public static bool operator !=(Point3D ponto1, object objeto)
        {
            return !(ponto1 == objeto);
        }

        public static bool operator ==(Point3D ponto1, Point3D ponto2)
        {
            if ((object)ponto1 == null)
                return ((object)ponto2 == null);

            if ((object)ponto2 == null)
                return false;

            return (ponto1.X == ponto2.X && ponto1.Y == ponto2.Y && ponto1.Z == ponto2.Z);
        }

        public static bool operator ==(Point3D ponto1, float valor)
        {
            return (ponto1.X == valor && ponto1.Y == valor && ponto1.Z == valor);
        }

        public static bool operator !=(Point3D ponto1, Point3D ponto2)
        {
            return !(ponto1 == ponto2);
        }

        public static bool operator !=(Point3D ponto1, float valor)
        {
            return !(ponto1 == valor);
        }

        public bool EqualsPoints(Point3D point)
        {
            if (MathFuncs.IsClose(X, point.X) &&
                MathFuncs.IsClose(Y, point.Y) &&
                MathFuncs.IsClose(Z, point.Z))
                return true;
            return false;
        }

        public override bool Equals(object o)
        {
            return this == o;
        }

        public override int GetHashCode()
        {
            return (int)X * (int)Y * (int)Z;
        }

        public float Distancia()
        {
            return Distancia(this, Point3D.Zero);
        }

        public float Distancia(Point3D ponto)
        {
            return Distancia(this, ponto);
        }

        public float Distancia(Point2D ponto)
        {
            return Point2D.Distancia(this, ponto);
        }

        public static float Distancia(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            return Distancia(new Point3D(x1, y1, z1), new Point3D(x2, y2, z2));
        }

        public static float Distancia(Point3D ponto1, Point3D ponto2)
        {
            float c1 = ponto2.X - ponto1.X;
            float c2 = ponto2.Y - ponto1.Y;
            float c3 = ponto2.Z - ponto1.Z;

            //return (float)Math.Sqrt( (c1 * c1) + (c2 * c2) + (c3 * c3) );
            return (float)Math.Sqrt(Math.Pow(c1, 2) + Math.Pow(c2, 2) + Math.Pow(c3, 2));
        }

        public static float Distancia(Point3D ponto1, Point3D ponto2, Angle anguloBase)
        {
            float hip = Point3D.Distancia(ponto1, ponto2);
            Angle angulo = Angle.EntreDoisPontos(ponto1, ponto2) - anguloBase;
            return (float)angulo.Seno * hip;
        }

        public void Zerar()
        {
            m_x = m_y = m_z = 0;
        }

        public virtual void Copiar(Point3D ponto)
        {
            m_x = ponto.X;
            m_y = ponto.Y;
            m_z = ponto.Z;
        }

        public virtual void Copiar(Point2D ponto)
        {
            m_x = ponto.X;
            m_y = ponto.Y;
        }

        public virtual void Copiar(float x, float y, float z)
        {
            m_x = x;
            m_y = y;
            m_z = z;
        }

        public void Polar(Point3D pontoOrigem, Angle alfa, Angle beta, float distancia)
        {
            Point3D resultado;

            resultado = pontoOrigem.Transladar(new Vector3D(alfa, beta), distancia);

            Copiar(resultado);
        }

        public Point3D Polar(Angle angulo, float distancia)
        {
            float x, y;

            x = X;
            y = Y;

            x = x + ((float)angulo.Cosseno * distancia);
            y = y + ((float)angulo.Seno * distancia);

            return new Point3D(x, y, Z);
        }

        public void Polar(Point3D pontoOrigem, Angle angulo, float distancia)
        {
            float x, y;

            x = pontoOrigem.X;
            y = pontoOrigem.Y;

            x = x + ((float)angulo.Cosseno * distancia);
            y = y + ((float)angulo.Seno * distancia);

            X = x;
            Y = y;
        }

        public Point3D Transladar(Vector3D vetor, float distancia)
        {
            return this + (vetor * distancia);
        }

        public void Transladar(float dx, float dy, float dz)
        {
            X += dx;
            Y += dy;
            Z += dz;
        }

        public void Transladar(Vector3D normal, Angle rotacao, float dx, float dy, float dz)
        {
            Point3D pt = new Point3D(dx, dy, dz);
            pt.Rotacionar(new Vector3D(0, 0, 1), rotacao);
            pt.Rotacionar(new Vector3D(0, 0, 1), normal);
            X += pt.X;
            Y += pt.Y;
            Z += pt.Z;
        }

        public void Rotacionar(Vector3D u, Vector3D v)
        {
            Rotacionar(u.ProdutoVetorial(v), u.Angulo(v), new Point3D(0, 0, 0));
        }

        public void Rotacionar(Vector3D u, Vector3D v, Point3D pontoBase)
        {
            Rotacionar(u.ProdutoVetorial(v), u.Angulo(v), pontoBase);
        }

        public void Rotacionar(Vector3D eixo, Angle angulo)
        {
            Rotacionar(eixo, angulo, new Point3D(0, 0, 0));
        }

        public void Rotacionar(Vector3D eixo, Angle angulo, Point3D pontoBase)
        {
            Vector3D eixoclone = (Vector3D)eixo.Clone();
            Angle ang = new Angle(360 - angulo.Degradianos);
            float c = (float)ang.Cosseno;
            float s = (float)ang.Seno;
            float ic = 1.0f - c;
            float[,] m = new float[3, 3];
            float x, y, z;

            eixoclone.Normalizar();

            x = pontoBase.X;
            y = pontoBase.Y;
            z = pontoBase.Z;

            m[0, 0] = (eixoclone.A * eixoclone.A) * ic + c;
            m[0, 1] = (eixoclone.B * eixoclone.A) * ic + (eixoclone.C * s);
            m[0, 2] = (eixoclone.C * eixoclone.A) * ic - (eixoclone.B * s);
            m[1, 0] = (eixoclone.A * eixoclone.B) * ic - (eixoclone.C * s);
            m[1, 1] = (eixoclone.B * eixoclone.B) * ic + c;
            m[1, 2] = (eixoclone.C * eixoclone.B) * ic + (eixoclone.A * s);
            m[2, 0] = (eixoclone.A * eixoclone.C) * ic + (eixoclone.B * s);
            m[2, 1] = (eixoclone.B * eixoclone.C) * ic - (eixoclone.A * s);
            m[2, 2] = (eixoclone.C * eixoclone.C) * ic + c;

            X -= x;
            Y -= y;
            Z -= z;

            x = X * m[0, 0] + Y * m[0, 1] + Z * m[0, 2] + x;
            y = X * m[1, 0] + Y * m[1, 1] + Z * m[1, 2] + y;
            z = X * m[2, 0] + Y * m[2, 1] + Z * m[2, 2] + z;

            X = x;
            Y = y;
            Z = z;
        }

        public void RotacionarOtimizado(Vector3D eixo, Angle angulo, Point3D pontoBase)
        {
            double angRad = MathFuncs.Rad(360 - angulo.Degradianos);

            float c = (float)Math.Cos(angRad);
            float s = (float)Math.Sin(angRad);
            float ic = 1.0f - c;

            float x = pontoBase.X, y = pontoBase.Y, z = pontoBase.Z;

            float AS = eixo.A * s;
            float BS = eixo.B * s;
            float CS = eixo.C * s;

            X -= x;
            Y -= y;
            Z -= z;

            //x = X * (((float)Math.Pow(eixo.A, 2)) * ic + c) + Y * ((eixo.B * eixo.A) * ic + CS) + Z * ((eixo.C * eixo.A) * ic - BS) + x;
            //y = X * ((eixo.A * eixo.B) * ic - CS) + Y * (((float)Math.Pow(eixo.B, 2)) * ic + c) + Z * ((eixo.C * eixo.B) * ic + AS) + y;
            //z = X * ((eixo.A * eixo.C) * ic + BS) + Y * ((eixo.B * eixo.C) * ic - AS) + Z * (((float)Math.Pow(eixo.C, 2)) * ic + c) + z;

            var abXic = eixo.B * eixo.A * ic;
            var acXic = eixo.C * eixo.A * ic;
            var bcXic = eixo.C * eixo.B * ic;
            x = X * (eixo.A * eixo.A * ic + c) + Y * (abXic + CS) + Z * (acXic - BS) + x;
            y = X * (abXic - CS) + Y * (eixo.B * eixo.B * ic + c) + Z * (bcXic + AS) + y;
            z = X * (acXic + BS) + Y * (bcXic - AS) + Z * (eixo.C * eixo.C * ic + c) + z;

            X = x;
            Y = y;
            Z = z;
        }

        public void DecrementarPosicao(Position pos)
        {
            X -= pos.PontoBase.X;
            Y -= pos.PontoBase.Y;
            Z -= pos.PontoBase.Z;

            //Vetor3D v = new Vetor3D(0, 0, -1);
            if (Math.Abs(pos.Normal.C) != 1)
                RotacionarOtimizado(VetorMenosZ, pos.Normal.AnguloPlano, Point3D.Zero);

            //v.Setar(0f, -1f, 0f);
            RotacionarOtimizado(VetorMenosY, pos.Normal.AnguloInclinacao, Point3D.Zero);

            //v.Setar(0f, 0f, -1f);
            RotacionarOtimizado(VetorMenosZ, pos.Rotacao, Point3D.Zero);
        }

        public void IncrementarPosicao(Position pos)
        {
            //Vetor3D v = new Vetor3D(0f, 0f, 1f);
            RotacionarOtimizado(VetorZ, pos.Rotacao, Zero);

            //v.Setar(0f, 1f, 0f);

            RotacionarOtimizado(VetorY, pos.Normal.AnguloInclinacao, Zero);

            if (Math.Abs(pos.Normal.C) != 1)
            {
                //v.Setar(0f, 0f, 1f);
                RotacionarOtimizado(VetorZ, pos.Normal.AnguloPlano, Zero);
            }

            X += pos.PontoBase.X;
            Y += pos.PontoBase.Y;
            Z += pos.PontoBase.Z;
        }

        private static readonly Vector3D VetorZ = new Vector3D(0f, 0f, 1f);
        private static readonly Vector3D VetorY = new Vector3D(0f, 1f, 0f);

        private static readonly Vector3D VetorMenosZ = new Vector3D(0f, 0f, -1f);
        private static readonly Vector3D VetorMenosY = new Vector3D(0f, -1f, 0f);

        private static Point3D m_zero;

        public static Point3D Zero
        {
            get
            {
                if (m_zero == null)
                    m_zero = new Point3D();
                return m_zero;
            }
        }

        public static Point3D operator +(Point3D ponto1, Point3D ponto2)
        {
            return new Point3D(ponto1.X + ponto2.X, ponto1.Y + ponto2.Y, ponto1.Z + ponto2.Z);
        }

        public static Point3D operator +(Point3D ponto, Vector3D vetor)
        {
            return new Point3D(ponto.X + vetor.A, ponto.Y + vetor.B, ponto.Z + vetor.C);
        }

        public static Point3D operator -(Point3D ponto1, Point3D ponto2)
        {
            return new Point3D(ponto1.X - ponto2.X, ponto1.Y - ponto2.Y, ponto1.Z - ponto2.Z);
        }

        public static Point3D operator -(Point3D ponto, Vector3D vetor)
        {
            return new Point3D(ponto.X - vetor.A, ponto.Y - vetor.B, ponto.Z - vetor.C);
        }

        public static Point3D operator -(Vector3D vetor, Point3D ponto)
        {
            return new Point3D(vetor.A - ponto.X, vetor.B - ponto.Y, vetor.C - ponto.Z);
        }

        public static Point3D operator *(Point3D ponto1, Point3D ponto2)
        {
            return new Point3D(ponto1.X * ponto2.X, ponto1.Y * ponto2.Y, ponto1.Z * ponto2.Z);
        }

        public static Point3D operator *(Point3D ponto, float fator)
        {
            return new Point3D(ponto.X * fator, ponto.Y * fator, ponto.Z * fator);
        }

        public static Point3D operator /(Point3D ponto1, Point3D ponto2)
        {
            return new Point3D(ponto1.X / ponto2.X, ponto1.Y / ponto2.Y, ponto1.Z / ponto2.Z);
        }

        public static Point3D operator /(Point3D ponto, float fator)
        {
            return new Point3D(ponto.X / fator, ponto.Y / fator, ponto.Z / fator);
        }

        public override string ToString()
        {
            return string.Concat(Math.Round(m_x, 3), "; ", Math.Round(m_y, 3), "; ", Math.Round(m_z, 3));
        }

        public static implicit operator Point2D(Point3D ponto)
        {
            if (ponto != null)
                return new Point2D(ponto.X, ponto.Y);
            else
                return null;
        }

        public static explicit operator Point3D(Point2D ponto)
        {
            return new Point3D(ponto);
        }
    }
}