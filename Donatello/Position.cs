using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace openGl_Control
{
    public class Position : Plan
    {
        private Angle m_rotacao;

        public Position(Plan plano, Angle rotacao)
            : base(plano.Normal, plano.PontoBase)
        {
            m_rotacao = rotacao;
        }

        public Position(Point3D pontoBase, Angle rotacao)
            : base(new Vector3D(0, 0, 1), pontoBase)
        {
            m_rotacao = rotacao;
        }

        public Position(Point3D pontoBase)
            : this(pontoBase, new Angle())
        {
        }

        public Position()
            : this(new Point3D(0, 0, 0), new Angle())
        {
        }

        public Angle Rotacao
        {
            get
            {
                return m_rotacao;
            }
            set
            {
                m_rotacao = value;
            }
        }

        public void Rotacionar(Angle deltaRotacao, Vector3D vetorRotacao, Point3D pontoBase)
        {
            this.PontoBase.Rotacionar(vetorRotacao, deltaRotacao, pontoBase);
            this.Rotacionar(deltaRotacao, vetorRotacao);
        }

        public void Rotacionar(Angle deltaRotacao, Vector3D vetorRotacao)
        {
            Vector3D vn = (Vector3D)this.Normal.Clone();
            Vector3D vr = this.DirecaoPerpendicularNormal(this.Rotacao);
            vn.Rotacionar(vetorRotacao, deltaRotacao);
            vr.Rotacionar(vetorRotacao, deltaRotacao);

            this.Normal = vn;
            Vector3D vrn = this.DirecaoPerpendicularNormal();

            vr.DecrementarVetor(this.Normal);
            vrn.DecrementarVetor(this.Normal);

            float ang1 = (float)Angle.EntreDoisPontos(0, 0, vrn.A, vrn.B).Degradianos;
            float ang2 = (float)Angle.EntreDoisPontos(0, 0, vr.A, vr.B).Degradianos;
            if (ang2 < ang1) ang2 += 360;
            this.Rotacao.Degradianos = ang2 - ang1;
            if (this.Normal.C == -1) this.Rotacao.Degradianos = -this.Rotacao.Degradianos;
        }

        public void IncrementaPosicao(Position posicaoPai)
        {
            //Para guardar o valor da direcao perpendicular a Normal (Rotacao)
            Vector3D vetperp = this.DirecaoPerpendicularNormal(this.Rotacao);

            // Para incrementar o PontoBase
            this.PontoBase.IncrementarPosicao(posicaoPai);

            // Para incrementar a Normal
            Position normalpai = new Position(new Point3D(0, 0, 0));
            normalpai.Rotacao = posicaoPai.Rotacao;
            normalpai.Normal = posicaoPai.Normal;

            Point3D pnormal = new Point3D(this.Normal.A, this.Normal.B, this.Normal.C);
            pnormal.IncrementarPosicao(normalpai);
            this.Normal.Setar(pnormal.X, pnormal.Y, pnormal.Z);

            // Para incrementar a Rota��o
            Point3D pontoperp = new Point3D(vetperp.A, vetperp.B, vetperp.C);
            pontoperp.IncrementarPosicao(normalpai);
            vetperp.Setar(pontoperp.X, pontoperp.Y, pontoperp.Z);

            Vector3D vetperpI = this.DirecaoPerpendicularNormal();

            vetperp.DecrementarVetor(this.Normal);
            vetperpI.DecrementarVetor(this.Normal);

            float ang1 = (float)Angle.EntreDoisPontos(0, 0, vetperpI.A, vetperpI.B).Degradianos;
            float ang2 = (float)Angle.EntreDoisPontos(0, 0, vetperp.A, vetperp.B).Degradianos;
            if (ang2 < ang1) ang2 += 360;
            this.Rotacao.Degradianos = ang2 - ang1;
            if (this.Normal.C == -1) this.Rotacao.Degradianos = -this.Rotacao.Degradianos;
        }

        public void DecrementaPosicao(Position posicaoPai)
        {
            //Para guardar o valor da direcao perpendicular a Normal (Rotacao)
            Vector3D vetperp = this.DirecaoPerpendicularNormal(this.Rotacao);

            // Para incrementar o PontoBase
            this.PontoBase.DecrementarPosicao(posicaoPai);

            // Para incrementar a Normal
            Position normalpai = new Position(new Point3D(0, 0, 0));
            normalpai.Rotacao = posicaoPai.Rotacao;
            normalpai.Normal = posicaoPai.Normal;

            Point3D pnormal = new Point3D(this.Normal.A, this.Normal.B, this.Normal.C);
            pnormal.DecrementarPosicao(normalpai);
            this.Normal.Setar(pnormal.X, pnormal.Y, pnormal.Z);
            this.Normal.Normalizar();

            // Para incrementar a Rota��o
            Point3D pontoperp = new Point3D(vetperp.A, vetperp.B, vetperp.C);
            pontoperp.DecrementarPosicao(normalpai);
            vetperp.Setar(pontoperp.X, pontoperp.Y, pontoperp.Z);
            vetperp.Normalizar();

            Vector3D vetperpI = this.DirecaoPerpendicularNormal();

            vetperp.DecrementarVetor(this.Normal);
            vetperpI.DecrementarVetor(this.Normal);

            float ang1 = (float)Angle.EntreDoisPontos(0, 0, vetperpI.A, vetperpI.B).Degradianos;
            float ang2 = (float)Angle.EntreDoisPontos(0, 0, vetperp.A, vetperp.B).Degradianos;
            if (ang2 < ang1) ang2 += 360;
            this.Rotacao.Degradianos = ang2 - ang1;
            if (this.Normal.C == -1) this.Rotacao.Degradianos = -this.Rotacao.Degradianos;
        }

        public virtual void EspelharApenasPosicao(Position posicaoEspelho)
        {
            DecrementaPosicao(posicaoEspelho);
            PontoBase.X = -PontoBase.X;
            Rotacao.Degradianos = -Rotacao.Degradianos;
            IncrementaPosicao(posicaoEspelho);
        }

        public virtual void Espelhar(Position posicaoEspelho)
        {
            DecrementaPosicao(posicaoEspelho);
            float normalC = this.Normal.C;
            PontoBase.X = -PontoBase.X;
            Rotacao.Degradianos = -Rotacao.Degradianos;
            if (Math.Abs(normalC) != 1)
            {
                Normal.Setar(-Normal.A, Normal.B, Normal.C);
                Rotacao.Degradianos += 180;
            }
            IncrementaPosicao(posicaoEspelho);
        }
                
        public void SetarCoordenadasPontoBase(float x, float y, float z)
        {
            this.PontoBase.X = x;
            this.PontoBase.Y = y;
            this.PontoBase.Z = z;
        }

        public void SetarValoresPosicao(Position posicao)
        {
            this.Normal.Setar(posicao.Normal.A, posicao.Normal.B, posicao.Normal.C);

            this.Rotacao.Degradianos = Math.Round(posicao.Rotacao.Degradianos,2);

            SetarCoordenadasPontoBase(posicao.PontoBase.X, posicao.PontoBase.Y, posicao.PontoBase.Z);
        }
    }
}