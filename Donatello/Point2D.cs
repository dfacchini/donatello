using openGl_Control.Math_Funcs;
using System;
using System.Drawing;

namespace openGl_Control
{
    public class Point2D
    {
        protected float m_x, m_y;

        public Point2D()
        {
            m_x = m_y = 0;
        }

        public Point2D(float x, float y)
        {
            m_x = x;
            m_y = y;
        }

        public virtual float X
        {
            get
            {
                return m_x;
            }
            set
            {
                m_x = value;
            }
        }

        public virtual float Y
        {
            get
            {
                return m_y;
            }
            set
            {
                m_y = value;
            }
        }

        public static implicit operator System.Drawing.PointF(Point2D ponto)
        {
            PointF pf = new PointF(ponto.X, ponto.Y);
            return pf;
        }

        public static explicit operator Point2D(PointF pontoF)
        {
            Point2D ponto = new Point2D(pontoF.X, pontoF.Y);
            return ponto;
        }

        public static float Distancia(float x1, float y1, float x2, float y2)
        {
            float op = (y2 - y1);
            float ad = (x2 - x1);

            return (float)Math.Sqrt((op * op) + (ad * ad));
        }

        public float Distancia(float x1, float y1)
        {
            float op = m_y - y1;
            float ad = m_x - x1;

            return (float)Math.Sqrt((op * op) + (ad * ad));
        }

        public float Distancia()
        {
            return Distancia(this, new Point2D(0, 0));
        }

        public virtual float Distancia(Point2D ponto)
        {
            return Distancia(this, ponto);
        }

        public virtual void Copiar(Point2D ponto)
        {
            this.X = ponto.X;
            this.Y = ponto.Y;
        }

        public virtual void Copiar(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public static float Distancia(Point2D ponto1, Point2D ponto2)
        {
            return (Point2D.Distancia(ponto1.X, ponto1.Y, ponto2.X, ponto2.Y));
        }

        public static float Distancia(Point2D ponto1, Point2D ponto2, Angle anguloBase)
        {
            float hip = Point2D.Distancia(ponto1, ponto2);
            Angle angulo = Angle.EntreDoisPontos(ponto1, ponto2) - anguloBase;
            return (float)angulo.Seno * hip;
        }

        //public static float Distancia(Linha linha, Ponto2D ponto)
        //{
        //    float d1, d2;
        //    Ponto2D p = Linha.PontoProjetadoNaReta(linha, ponto, false);

        //    if (p == null)
        //        return (d1 = linha.PontoI.Distancia(ponto)) < (d2 = linha.PontoF.Distancia(ponto)) ? d1 : d2;
        //    else
        //        return Distancia(p, ponto);
        //}

        public static float AnguloEntre(Point2D A, Point2D B, Point2D C, AngleBetween returnedAngle)
        {
            // lei dos cossenos
            // a� = b� + c� - 2bc * cos(A)
            // b� = a� + c� - 2ac * cos(B)
            // c� = a� + b� - 2ab * cos(C)

            float a = Distancia(B, C);
            float b = Distancia(A, C);
            float c = Distancia(A, B);

            if (returnedAngle == AngleBetween.A)
            {
                //cos(A) = (-a�+b�+c�)/2bc

                double cosA = (-Math.Pow(a, 2) + Math.Pow(b, 2) + Math.Pow(c, 2)) / (2 * b * c);
                return (float)Math.Acos(cosA);
            }
            else if (returnedAngle == AngleBetween.B)
            {
                //cos(B) = (-b�+a�+c�)/2ac

                double cosB = (-Math.Pow(b, 2) + Math.Pow(a, 2) + Math.Pow(c, 2)) / (2 * a * c);
                return (float)Math.Acos(cosB);
            }
            else if (returnedAngle == AngleBetween.C)
            {
                //cos(C) = (-c�+a�+b�)/2ab

                double cosC = (-Math.Pow(c, 2) + Math.Pow(a, 2) + Math.Pow(b, 2)) / (2 * a * b);
                return (float)Math.Acos(cosC);
            }

            return 0f;
        }

        public virtual void Zerar()
        {
            m_x = m_y = 0;
        }

        public Point2D Polar(Angle angulo, float distancia)
        {
            return this.Polar(angulo.Degradianos, distancia);
        }

        public Point2D Polar(double anguloDegradiano, float distancia)
        {
            double anguloRadiano = anguloDegradiano * Math.PI / 180;
            float x, y;

            x = this.X;
            y = this.Y;

            x += (float)Math.Round(Math.Cos(anguloRadiano) * distancia, 7);
            y += (float)Math.Round(Math.Sin(anguloRadiano) * distancia, 7);

            return new Point2D(x, y);
        }

        public void Polar(Point2D pontoOrigem, Angle angulo, float distancia)
        {
            Point2D resultado;

            resultado = pontoOrigem.Polar(angulo, distancia);

            Copiar(resultado);
        }

        public void Polar(Point2D pontoOrigem, double anguloDegradiano, float distancia)
        {
            Point2D resultado;

            resultado = pontoOrigem.Polar(anguloDegradiano, distancia);

            Copiar(resultado);
        }

        public static Point2D operator +(Point2D ponto1, Point2D ponto2)
        {
            return new Point2D(ponto1.X + ponto2.X, ponto1.Y + ponto2.Y);
        }

        public static Point2D operator -(Point2D ponto1, Point2D ponto2)
        {
            return new Point2D(ponto1.X - ponto2.X, ponto1.Y - ponto2.Y);
        }

        public static Point2D operator *(Point2D ponto1, Point2D ponto2)
        {
            return new Point2D(ponto1.X * ponto2.X, ponto1.Y * ponto2.Y);
        }

        public static Point2D operator *(Point2D ponto, float fator)
        {
            return new Point2D(ponto.X * fator, ponto.Y * fator);
        }

        public static Point2D operator *(float fator, Point2D ponto)
        {
            return new Point2D(ponto.X * fator, ponto.Y * fator);
        }

        public static double operator |(Point2D ponto1, Point2D ponto2)
        {
            return (ponto1.X * ponto2.Y) + (ponto1.Y * ponto2.Y);
        }

        public static double operator ^(Point2D ponto1, Point2D ponto2)
        {
            return (ponto1.X * ponto2.Y) - (ponto1.X * ponto2.Y);
        }

        public static Point2D operator /(Point2D ponto1, Point2D ponto2)
        {
            return new Point2D(ponto1.X / ponto2.X, ponto1.Y / ponto2.Y);
        }

        public static Point2D operator /(Point2D ponto, float fator)
        {
            return new Point2D(ponto.X / fator, ponto.Y / fator);
        }

        public static bool operator ==(Point2D ponto1, Point2D ponto2)
        {
            if ((object)ponto1 == null)
                return ((object)ponto2 == null);

            if ((object)ponto2 == null)
                return false;

            return MathFuncs.IsClose(ponto1.X, ponto2.X) &&
                   MathFuncs.IsClose(ponto1.Y, ponto2.Y);
        }

        public static bool operator !=(Point2D ponto1, Point2D ponto2)
        {
            return !(ponto1 == ponto2);
        }

        public override bool Equals(object o)
        {
            if (o is Point2D)
                return this == (Point2D)o;

            return false;
        }

        public override string ToString()
        {
            return string.Concat(Math.Round(this.m_x, 3), "; ", Math.Round(this.m_y, 3));
        }

        public static Point2D Interseccao2D(Point2D ponto1, Angle angulo1, Point2D ponto2, Angle angulo2)
        {
            float denominador = (float)angulo1.Tangente - (float)angulo2.Tangente;
            Point2D interseccao = new Point2D();

            if (MathFuncs.IsClose(denominador, 0))
                return ponto1;//return ponto2;

            //if ( FuncoesMatematicas.ValorEqual(angulo1.Degradianos, 90 , 0.1) || FuncoesMatematicas.ValorEqual(angulo1.Degradianos, 270 , 0.01) )
            //{
            //    interseccao.X = ponto1.X;
            //    interseccao.Y = (((float)angulo2.Tangente * ponto1.X) - ((float)angulo2.Tangente * ponto2.X) + ponto2.Y);
            //}
            //else if ( FuncoesMatematicas.ValorEqual(angulo2.Degradianos, 90 , 0.1) || FuncoesMatematicas.ValorEqual(angulo2.Degradianos, 270 , 0.01) )
            //{
            //    interseccao.X = ponto2.X;
            //    interseccao.Y = (((float)angulo1.Tangente * ponto2.X) - ((float)angulo1.Tangente * ponto1.X) + ponto1.Y);
            //}
            //else
            //{
            //    interseccao.X = (((float)angulo1.Tangente * ponto1.X) - ponto1.Y - ((float)angulo2.Tangente * ponto2.X) + ponto2.Y) / denominador;
            //    interseccao.Y = (((float)angulo1.Tangente * interseccao.X) - ((float)angulo1.Tangente * ponto1.X) + ponto1.Y);
            //}

            if (MathFuncs.IsClose(angulo1.Degradianos, 90) || MathFuncs.IsClose(angulo1.Degradianos, 270))
            {
                interseccao.X = ponto1.X;
                interseccao.Y = (((float)angulo2.Tangente * ponto1.X) - ((float)angulo2.Tangente * ponto2.X) + ponto2.Y);
            }
            else if (MathFuncs.IsClose(angulo2.Degradianos, 90) || MathFuncs.IsClose(angulo2.Degradianos, 270))
            {
                interseccao.X = ponto2.X;
                interseccao.Y = (((float)angulo1.Tangente * ponto2.X) - ((float)angulo1.Tangente * ponto1.X) + ponto1.Y);
            }
            else
            {
                interseccao.X = (((float)angulo1.Tangente * ponto1.X) - ponto1.Y - ((float)angulo2.Tangente * ponto2.X) + ponto2.Y) / denominador;
                interseccao.Y = (((float)angulo1.Tangente * interseccao.X) - ((float)angulo1.Tangente * ponto1.X) + ponto1.Y);
            }
            return interseccao;
        }

        public void InverterXY(bool inverterX, bool inverterY)
        {
            if (inverterX)
                this.X = -this.X;
            if (inverterY)
                this.Y = -this.Y;
        }
    }

    public enum AngleBetween
    {
        A,
        B,
        C
    }
}