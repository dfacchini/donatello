﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace openGl_Control.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("openGl_Control.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;
        ///
        ///void main(void)
        ///{
        ///   vec3 L = normalize(gl_LightSource[0].position.xyz - v);
        ///   vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);
        ///   Idiff = clamp(Idiff, 0.0, 1.0); 
        ///
        ///   gl_FragColor = Idiff;
        ///}.
        /// </summary>
        internal static string light_frag {
            get {
                return ResourceManager.GetString("light_frag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;
        ///
        ///void main(void)
        ///{
        ///
        ///   v = vec3(gl_ModelViewMatrix * gl_Vertex);       
        ///   N = normalize(gl_NormalMatrix * gl_Normal);
        ///   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
        ///}.
        /// </summary>
        internal static string light_vert {
            get {
                return ResourceManager.GetString("light_vert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;    
        ///void main (void)  
        ///{  
        ///   vec3 L = normalize(gl_LightSource[0].position.xyz - v);   
        ///   vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  
        ///   vec3 R = normalize(-reflect(L,N));  
        /// 
        ///   //calculate Ambient Term:  
        ///   vec4 Iamb = gl_FrontLightProduct[0].ambient;    
        ///
        ///   //calculate Diffuse Term:  
        ///   vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);
        ///   Idiff = clamp(Idiff, 0.0, 1.0);     
        ///   
        ///   // calculate Specular  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string pointLight_frag {
            get {
                return ResourceManager.GetString("pointLight_frag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;
        ///void main(void)  
        ///{     
        ///   v = vec3(gl_ModelViewMatrix * gl_Vertex);       
        ///   N = normalize(gl_NormalMatrix * gl_Normal);
        ///   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;  
        ///}.
        /// </summary>
        internal static string pointLight_vert {
            get {
                return ResourceManager.GetString("pointLight_vert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to #version 330
        ///
        ///uniform  vec4 rgba;
        ///
        ///void main (void)
        ///{
        ///	gl_FragColor = rgba;
        ///}.
        /// </summary>
        internal static string selection_frag {
            get {
                return ResourceManager.GetString("selection_frag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to //#version 330
        ///
        /////layout (std140) uniform Matrices {
        ///// mat4 m_pvm;
        ///// mat4 m_viewModel;
        ///// mat3 m_normal;
        /////};
        ///
        /////in vec4 position;
        ///
        /////void main()
        /////{
        ///// gl_Position = m_pvm * position ;
        /////}
        ///
        ///void main(void)
        ///{
        ///	vec4 a = gl_Vertex;
        ///	//a.x = a.x * 0.2;
        ///	//a.y = a.y * 0.2;
        ///	gl_Position = gl_ModelViewProjectionMatrix * a;
        ///}.
        /// </summary>
        internal static string selection_vert {
            get {
                return ResourceManager.GetString("selection_vert", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;    
        ///void main (void)  
        ///{  
        ///   vec3 L = normalize(gl_LightSource[0].position.xyz - v);   
        ///   vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  
        ///   vec3 R = normalize(-reflect(L,N));  
        /// 
        ///   //calculate Ambient Term:  
        ///   vec4 Iamb = gl_FrontLightProduct[0].ambient;    
        ///
        ///   //calculate Diffuse Term:  
        ///   vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);
        ///   Idiff = clamp(Idiff, 0.0, 1.0);     
        ///   
        ///   // calculate Specular  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string smoothLine_frag {
            get {
                return ResourceManager.GetString("smoothLine_frag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to varying vec3 N;
        ///varying vec3 v;
        ///void main(void)  
        ///{     
        ///   v = vec3(gl_ModelViewMatrix * gl_Vertex);       
        ///   N = normalize(gl_NormalMatrix * gl_Normal);
        ///   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;  
        ///}.
        /// </summary>
        internal static string smoothLine_vert {
            get {
                return ResourceManager.GetString("smoothLine_vert", resourceCulture);
            }
        }
    }
}
