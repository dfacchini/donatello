﻿namespace openGl_Control
{
    partial class MainOpenglControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleOpenGl_control = new Tao.Platform.Windows.SimpleOpenGlControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkPicking = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // simpleOpenGl_control
            // 
            this.simpleOpenGl_control.AccumBits = ((byte)(0));
            this.simpleOpenGl_control.AutoCheckErrors = false;
            this.simpleOpenGl_control.AutoFinish = false;
            this.simpleOpenGl_control.AutoMakeCurrent = true;
            this.simpleOpenGl_control.AutoSwapBuffers = false;
            this.simpleOpenGl_control.BackColor = System.Drawing.Color.Black;
            this.simpleOpenGl_control.ColorBits = ((byte)(32));
            this.simpleOpenGl_control.DepthBits = ((byte)(16));
            this.simpleOpenGl_control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleOpenGl_control.Location = new System.Drawing.Point(3, 26);
            this.simpleOpenGl_control.Name = "simpleOpenGl_control";
            this.simpleOpenGl_control.Size = new System.Drawing.Size(778, 532);
            this.simpleOpenGl_control.StencilBits = ((byte)(0));
            this.simpleOpenGl_control.TabIndex = 2;
            this.simpleOpenGl_control.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MouseDown);
            this.simpleOpenGl_control.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MouseMove);
            this.simpleOpenGl_control.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MouseUp);
            this.simpleOpenGl_control.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.MouseWheel);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.simpleOpenGl_control, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.chkPicking, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 561);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // chkPicking
            // 
            this.chkPicking.AutoSize = true;
            this.chkPicking.Location = new System.Drawing.Point(5, 3);
            this.chkPicking.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.chkPicking.Name = "chkPicking";
            this.chkPicking.Size = new System.Drawing.Size(91, 17);
            this.chkPicking.TabIndex = 3;
            this.chkPicking.Text = "Picking Mode";
            this.chkPicking.UseVisualStyleBackColor = true;
            this.chkPicking.CheckedChanged += new System.EventHandler(this.chkPicking_CheckedChanged);
            // 
            // MainOpenglControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainOpenglControl";
            this.Text = "Donatello";
            this.Shown += new System.EventHandler(this.MainOpenglControl_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }      

        #endregion

        private Tao.Platform.Windows.SimpleOpenGlControl simpleOpenGl_control;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox chkPicking;
    }
}

