﻿using System;
using System.Windows.Forms;
using Tao.OpenGl;

namespace openGl_Control
{
    public partial class MainOpenglControl : Form
    {
        #region Properties

        private World world = null;

        private float _inRotationX, _inRotationy;
        private float RotationX, Rotationy;

        private float _rotationX, _rotationy;
        private bool mousePressed = false;
        private bool m_isCharging = false;        

        #endregion

        public MainOpenglControl(World world)
        {
            this.m_isCharging = true;
            if (world == null)
                throw new ArgumentNullException("world");

            this.world = world;

            InitializeComponent();
            this.simpleOpenGl_control.InitializeContexts();

            this.world.viewControl = this.simpleOpenGl_control;
            this.world.InitGl();
            this.world.InitPrimitives();
            this.world.SetView(this.Height, this.Width);
            this.m_isCharging = false;

            //int _frameBuffer;
            //Gl.glGenFramebuffersEXT(1, out _frameBuffer);
            //Gl.glBindFramebufferEXT(Gl.GL_FRAMEBUFFER_EXT, 0);
        }

        #region Override Methods

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (!this.m_isCharging)
            {
                this.world.SetView(this.Height, this.Width);
                this.Refresh();
                this.world.RenderScene();
            }
        }

        #endregion

        #region Events

        private void MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            var x = e.X;
            var y = e.Y;
            this.world.UpdateZ(e.Delta, x, y);
            this.Refresh();
            this.world.RenderScene();
        }

        private void MouseMove(object sender, MouseEventArgs e)
        {
            if (!mousePressed) return;

            float deltaX = _rotationX - e.X;
            float deltaY = _rotationy - e.Y;

            Rotationy = _inRotationy - deltaX / 3;
            RotationX = _inRotationX - deltaY / 3;

            this.world.UpdateLocations(RotationX, Rotationy);

            this.Refresh();

            this.world.RenderScene();
        }

        private void MouseDown(object sender, MouseEventArgs e)
        {
            Console.WriteLine("X = " + e.X + "Y = " + e.Y);

            _rotationX = e.X;

            _rotationy = e.Y;

            this._inRotationX = this.RotationX;
            this._inRotationy = this.Rotationy;

            this.mousePressed = true;

            this.Refresh();
            if (chkPicking.Checked)
                this.world.PickScene(e.X, e.Y);
            else
                this.world.RenderScene();
        }

        private void MouseUp(object sender, MouseEventArgs e)
        {
            this.mousePressed = false;
            this.Refresh();
            this.world.RenderScene();
        }

        private void MainOpenglControl_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            this.world.RenderScene();
        }

        private void chkPicking_CheckedChanged(object sender, EventArgs e)
        {
            this.world.InitPickMode();
        }

        #endregion
    }
}