﻿using System;
using NUnit.Framework;
using openGl_Control.Elements;
using openGl_Control;
using System.Collections.Generic;

namespace DonatelloTestProject
{
    [TestFixture]
    public class DrawTests
    {
        private List<Point3D> collectionXYP = new List<Point3D>
        {       new Point3D(0, 0, 0),
                new Point3D(100, 0, 0),
                new Point3D(100, 100, 0),
                new Point3D(0, 100, 0)
        };

        private List<Point3D> collectionExpected = new List<Point3D>
        {       new Point3D(-50, -50, 0),
                new Point3D(50, -50, 0),
                new Point3D(50, 50, 0),
                new Point3D(-50, 50, 0)
        };

        private List<Point3D> collectionXYP2 = new List<Point3D>
        {       new Point3D(100, 100, 0),
                new Point3D(200, 100, 0),
                new Point3D(200, 200, 0),
                new Point3D(100, 200, 0)
        };

        private List<Point3D> collectionXPYN = new List<Point3D>
        {       new Point3D(0, 0, 0),
                new Point3D(100, 0, 0),
                new Point3D(100, -100, 0),
                new Point3D(0, -100, 0)
        };

        private List<Point3D> collectionXYN = new List<Point3D>
        {       new Point3D(-100, -100, 0),
                new Point3D(-200, -100, 0),
                new Point3D(-200, -200, 0),
                new Point3D(-100, -200, 0)
        };


        private List<Point3D> collectionXNYP = new List<Point3D>
        {       new Point3D(-100, 100, 0),
                new Point3D(-200, 100, 0),
                new Point3D(-200, 200, 0),
                new Point3D(-100, 200, 0)
        };

        [Test]
        public void Test2DCentralizeXYP()
        {
            Polygon polygon = new Polygon();
            polygon.Centralize3D(collectionXYP, false);
            Assert.IsTrue(AssertCollections(collectionExpected, collectionXYP));


            polygon.Centralize3D(collectionXYP2, false);
            Assert.IsTrue(AssertCollections(collectionExpected, collectionXYP2));
        }

        [Test]
        public void Test2DCentralizeXPYN()
        {
            Polygon polygon = new Polygon();
            polygon.Centralize3D(collectionXPYN, false);
            Assert.IsTrue(AssertCollections(collectionExpected, collectionXPYN));
        }

        [Test]
        public void Test2DCentralizeXNYN()
        {
            Polygon polygon = new Polygon();
            polygon.Centralize3D(collectionXYN, false);
            Assert.IsTrue(AssertCollections(collectionExpected, collectionXYN));
        }

        [Test]
        public void Test2DCentralizeXNYP()
        {
            Polygon polygon = new Polygon();
            polygon.Centralize3D(collectionXNYP, false);
            Assert.IsTrue(AssertCollections(collectionExpected, collectionXNYP));
        }

        private bool AssertCollections(List<Point3D> points1, List<Point3D> points2)
        {
            for (int i = 0; i < points1.Count; i++)
            {
                bool contains = false;
                for (int j = 0; j < points2.Count; j++)
                {
                    if (points1[i].EqualsPoints(points2[j]))
                        contains = true;
                }

                if (!contains)
                    return false;
            }
            return true;
        }
    }
}