﻿using NUnit.Framework;
using openGl_Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonatelloTestProject
{
    [TestFixture]
    public class PointsTests
    {
        private Point3D point_1 = new Point3D(100.5f, 100.5f, 100.5f);
        private Point3D point_2 = new Point3D(100.5001f, 100.5001f, 100.5001f);
        private Point3D point_3 = new Point3D(100.50001f, 100.50001f, 100.50001f);

        [Test]
        public void TestPointsEquals()
        {
            Assert.IsFalse(point_1.EqualsPoints(point_2));
            Assert.IsTrue(point_1.EqualsPoints(point_3));

            Assert.IsFalse(point_2.EqualsPoints(point_3));
        }
    }
}
